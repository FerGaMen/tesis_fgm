(*
* Implementación de álgebras relacionales.
*
* Desarrollado por Fernando Abigail Galicia Mendoza
* Contacto: fernandogamen@ciencias.unam.mx
* Coq v8.7
* OCaml v4.05.0
*
* Trabajo desarrollado bajo el marco del proyecto:
* "Tópicos en computación teórica" (PAPIME, PE102117)
* Otorgado por la DGAPA de la UNAM
*
* Referencias:
* 1. Givant:
*    "The calculus of relations as a foundation of mathematics"
* 2. Huntington: 
*    "Sets of independent postulates for the algebra of logic"
* Una observación importante:
* Se optó por utilizar la primera axiomaticación de álgebras de Bool
* dada por Huntington, para los fines de este proyecto.
*)

Require Export Lib.Monoide.Mon.
Require Export Lib.AlgBool.AB.

(*
* Definimos un álgebra relacional como la tupla:
* <A,0',1',−,+,∙,e',∘,∽>, donde:
* <A,0',1',−,+,∙> es un álgebra de Bool.
* <A,e',∘> es un monoide.
* El operador ∽ es inverso de si mismo.
* Semi-distribución de la ∽ sobre la ∘.
* Distribución de la ∘ sobre la +.
* Distribución de la ∽ sobre la +.
* Se cumple la ecuación de Tarski-De Morgan.
*)
Class AR
      (A:Type)
      (cero uno e:A)
      (neg inv:A → A)
      (suma produ compo:A → A → A)
      `(AB:AlgBool A uno cero neg suma produ)
      `(M:MonS A e compo) :=
  {
    invI: ∀ (x:A),
      inv (inv x) = x;
    invComp: ∀ (x y:A),
        inv (compo x y) = compo (inv y) (inv x);
    sumProd: ∀ (x y z:A),
        compo (suma x y) z = suma (compo x z) (compo y z);
    invSum: ∀ (x y:A),
        inv (suma x y) = suma (inv x) (inv y);
    eTDM: ∀ (x y:A),
        suma (compo (inv x) (neg (compo x y))) (neg y) = neg y
  }.

Hint Resolve invI.
Hint Resolve invComp.
Hint Resolve sumProd.
Hint Resolve invSum.
Hint Resolve eTDM.