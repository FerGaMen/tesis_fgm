(*
* Implementación del lenguaje XPath.
*
* Desarrollado por Fernando Abigail Galicia Mendoza
* Contacto: fernandogamen@ciencias.unam.mx
* Coq v8.7
* OCaml v4.05.0
*
* Trabajo desarrollado bajo el marco del proyecto:
* "Tópicos en computación teórica" (PAPIME, PE102117)
* Otorgado por la DGAPA de la UNAM
*
* Referencias:
* 1. Ikeda y Nishimura:
*    "Calculating tree navigation with symmetric relational zipper."
*)

Require Export Lib.Zipper.Zipper_AR.
Require Export Lib.Rels.Rel_Neg.

(*Sintaxis de movimientos*)
Inductive axis : Type :=
| self : axis
| child : axis
| foll_sibling : axis
| desc : axis
| desc_or_self : axis
| following : axis
| parent : axis
| prec_sibling : axis
| anc : axis
| anc_or_self : axis
| preceding : axis.

(*Sintaxis de caminos*)
Inductive path {A:Type} : Type :=
| step : axis -> path
| stepNT : axis -> A -> path
| pComp : path -> path -> path
| pPred : path -> predicate -> path
with predicate {A:Type} : Type :=
     | pathP : path -> predicate
     | negation : predicate -> predicate
     | conjuction : predicate -> predicate -> predicate
     | disyunction : predicate -> predicate -> predicate.

Notation "a ; <*>" := (step a) (at level 80).
Notation "a ;; b" := (stepNT a b) (at level 80).
Notation "p [[ q ]]" := (pPred p q) (at level 80).
Notation "p // q" := (pComp p q) (at level 80).

(*Sintaxis del lenguaje xpath*)
Inductive xpath {A:Type} : Type :=
| aP : @path A -> xpath
| rP : @path A -> xpath
| unionP : xpath -> xpath -> xpath
| interP : xpath -> xpath -> xpath.

Notation "\ p" := (aP p) (at level 80).
Notation "p `" := (rP p) (at level 80).
Notation "(~) q" := (negation q) (at level 80).

(*Funciones de interpretación*)

(*Interpretación de movimientos*)
Definition ia {A:Type} (a:axis) : relation (zipper A) :=
  match a with
  | self => id
  | child => (dn_R <*>) <∘> dn_L
  | foll_sibling => dn_R <+>
  | desc => ((dn_R <*>) <∘> dn_L) <+>
  | desc_or_self => ((dn_R <*>) <∘> dn_L) <*>
  | following =>
    ((((dn_R <*>) <∘> dn_L) <*>) <∘> (dn_R <+>)) <∘>
                                ((up_L <∘> (up_R <*>)) <*>)
  | parent => up_L <∘> (up_R <*>)
  | prec_sibling => up_R <+>
  | anc => (up_L <∘> (up_R <*>)) <+>
  | anc_or_self => (up_L <∘> (up_R <*>)) <*>
  | preceding =>
    ((((dn_R <*>) <∘> dn_L) <*>) <∘> (up_R <+>)) <∘>
                                ((up_L <∘> (up_R <*>)) <*>)
  end.

(*Interpretación de caminos*)
Fixpoint ip {A:Type} (p:@path A) : relation (zipper A) :=
  match p with
  | step a => (nodeCoref <X> id) <∘> (ia a)
  | stepNT a b => (betaCoref b) <∘> (ia a)
  | pComp p1 p2 => (ip p2) <∘> (ip p1)
  | pPred p q => let IQ :=
    fix iq (q:predicate) : relation (zipper A) :=
       match q with
       | pathP p => dom (ip p)
       | negation q => (¬) (iq q)
       | conjuction q1 q2 => (iq q1) ∩ (iq q2)
       | disyunction q1 q2 => (iq q1) ∪ (iq q2)
       end in (IQ q) <∘> (ip p)
  end.

(*Interpretación de xpath*)
Fixpoint ie {A:Type} (e:xpath) : relation (zipper A) :=
  match e with
  | aP p => (ip p) <∘> (id <X> topCoref) <∘> ((up_L ∪ up_R) <*>)
  | rP p => ip p
  | unionP e1 e2 => (ie e1) ∪ (ie e2)
  | interP e1 e2 => (ie e1) ∩ (ie e2)
  end.

(*Propiedad "fácilmente" demostrable*)
Lemma symmAxes: ∀ (A:Type), (@ia A child) = ((@ia A parent)~).
Proof.
  intros;destruct_eqRel.
  -simpl in *;do 2 destruct H.
   exists x0;split.
   +rewrite <- UPDN_INVERSE_1;unfold inv;assumption.
   +rewrite <- UPDN_INVERSE_2;rewrite invcrt.
    unfold inv;assumption.
  -simpl in *;do 2 destruct H.
   exists x0;split.
   +rewrite <- UPDN_INVERSE_2 in H0.
    rewrite invcrt in H0;unfold inv in H0.
    assumption.
   +rewrite <- UPDN_INVERSE_1 in H.
    unfold inv in H.
    assumption.
Qed.

(*Ejemplo de función de interpretación*)
Example ej1:
  ∀ (A:Type) (b:A),
  @ie A ((parent ; <*> // (foll_sibling ; <*>) //
                    (child ;; b)) `) =
  (((betaCoref b) <∘> ((dn_R <*>) <∘> dn_L)) <∘>
   (((nodeCoref <X> id) <∘> (dn_R <+>)) <∘>
    ((nodeCoref <X> id) <∘> (up_L <∘> (up_R <*>))))).
Proof.
  simpl;reflexivity.
Qed.