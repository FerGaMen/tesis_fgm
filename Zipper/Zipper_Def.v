(*
* Implementación de la estructura del zipper.
*
* Desarrollado por Fernando Abigail Galicia Mendoza
* Contacto: fernandogamen@ciencias.unam.mx
* Coq v8.7
* OCaml v4.05.0
*
* Trabajo desarrollado bajo el marco del proyecto:
* "Tópicos en computación teórica" (PAPIME, PE102117)
* Otorgado por la DGAPA de la UNAM
*
* Referencias:
* 1. Huet:
*    "Functional Pearl: The zipper."
* 2 Ikeda y Nishimura:
*   "Calculating tree navigation with symmetric relational zipper."
*)

Require Import List.
Require Export Utf8.
Import ListNotations.
Open Scope list_scope.

Set Implicit Arguments.

(*Definición de árbol binario (tree).*)
Inductive tree (A:Type): Type :=
| Empty : tree A
| Node : tree A → A → tree A → tree A.

(*Definición de migaja (breadcumb).*)
Inductive breadcumb (A:Type): Type :=
| L : A → tree A → breadcumb A
| R : A → tree A → breadcumb A.

(*Definición de contexto (ctx).*)
Definition ctx (A:Type): Type := list (breadcumb A).

(*Definición de zipper.*)
Definition zipper (A:Type): Type := prod (tree A) (ctx A).

(*Función re navegación abajo-izquierra.*)
Definition dnL {A:Type} (z:zipper A) : option (zipper A) :=
  match z with
  | (Node l x r,c) => Some (l,L x r::c)
  | _ => None
  end.

(*Función de navegación arriba-izquierda.*)
Definition upL {A:Type} (z:zipper A) : option (zipper A) :=
  match z with
  | (l,L x r::c) => Some (Node l x r,c)
  | _ => None
  end.

(*Función de navegación abajo-derecha.*)
Definition dnR  {A:Type} (z:zipper A) : option (zipper A) :=
  match z with
  | (Node l x r,c) => Some (r,R x l::c)
  | _ => None
  end.

(*Función de navegación arriba-derecha.*)
Definition upR {A:Type} (z:zipper A) : option (zipper A) :=
  match z with
  | (r,R x l::c) => Some (Node l x r,c)
  | _ => None
  end.

(*Función de navegación arriba, depende del ctx.*)
Definition go_up {A:Type} (z:zipper A) : option (zipper A) :=
  match z with
  | (l,L x r::c) => Some (Node l x r,c)
  | (r,R x l::c) => Some (Node l x r,c)
  | _ => None
  end.