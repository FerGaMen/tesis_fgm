(*
* Propiedades sobre las relaciones binarias
* negativas.
*
* Desarrollado por Fernando Abigail Galicia Mendoza
* Contacto: fernandogamen@ciencias.unam.mx
* Coq v8.7
* OCaml v4.05.0
*
* Trabajo desarrollado bajo el marco del proyecto:
* "Tópicos en computación teórica" (PAPIME, PE102117)
* Otorgado por la DGAPA de la UNAM
*
* Referencias:
* 1. Ikeda y Nishimura:
*    "Calculating tree navigation with symmetric relational zipper."
*)

Require Export Lib.Rels.Rel_Def.
Require Export Lib.Rels.Rel_AR.
Require Export Lib.Rels.Rel_Prop.

(*Interpretación de la relación negativa.*)
Definition negR {A:Type} (R:relation A) := id \ R.

Notation "(¬) R" := (negR R) (at level 80).

(*Aspectos básicos de las relaciones negadas.*)

(*La negación es no monotona.*)
Proposition antimono: ∀ (A:Type) (R S:relation A),
  coref R → ((R ⊆ S) ↔ (((¬) S) ⊆ ((¬) R))).
Proof.
  unfold iff;split;intros.
  -unfold inclusion;intros.
   destruct H1.
   constructor.
   +assumption.
   +unfold not;intros;apply H2.
    apply H0;assumption.
  -unfold inclusion;intros.
   apply NNPP.
   unfold not;intros.
   assert (id x y).
   +apply H;assumption.
   +assert (((¬) S) x y).
    *constructor;assumption.
    *apply H0 in H4.
     destruct H4.
     contradiction.
Qed.

(*Las relaciones negativas son coreflexivas.*)
Proposition NEG_COREFL:∀ (A:Type) (R:relation A),
  coref ((¬) R).
Proof.
  intros;unfold coref;unfold inclusion;intros.
  destruct H;assumption.
Qed.

(*El complemento de la vacía es la identidad.*)
Lemma NEG_CMPL_1:∀ (A:Type),
  ((¬) @vacia A) = id.
Proof.
  intros;destruct_eqRel.
  -destruct H;assumption.
  -constructor.
   +assumption.
   +unfold not;intros.
    contradiction.
Qed.

(*El comeplemento de la identidad es la vacía.*)
Lemma NEG_CMPL_2: ∀ (A:Type),
  ((¬) id) = (@vacia A).
Proof.
  intros;destruct_eqRel.
  -destruct H.
   contradiction.
  -contradiction.
Qed.

(*
 * Si una relación es correflexiva, su doble negación
 * resulta en ella misma.
 *)
Lemma NEG_DBLNEG: ∀ (A:Type) (R:relation A),
  coref R → ((¬) ((¬) R)) = R.
Proof.
  intros;destruct_eqRel.
  -destruct H0.
   apply NNPP (*Necesaria la lógica clásica*).
   unfold not;intros.
   apply H1.
   constructor;assumption.
  -constructor.
   +apply H;assumption.
   +unfold not;intros.
    destruct H1.
    contradiction.
Qed.

(*Primera ley de De Morgan.*)
Lemma NEG_DEMORGAN_1: ∀ (A:Type) (R S:relation A),
  ((¬) (R ∪ S)) = (((¬) R) ∩ ((¬) S)).
Proof.
  intros;destruct_eqRel.
  -destruct H.
   split.
   +constructor.
    *assumption.
    *unfold not;intros.
     apply H0;left;assumption.
   +constructor.
    *assumption.
    *unfold not;intros.
     apply H0;right;assumption.
  -destruct H.
   destruct H.
   destruct H0.
   constructor.
   +assumption.
   +unfold not;intros.
    destruct H3.
    *apply H1;assumption.
    *apply H2;assumption.
Qed.

(*Segunda ley de De Morgan.*)
Lemma NEG_DEMORGAN_2: ∀ (A:Type) (R S:relation A),
  ((¬) (R ∩ S)) = (((¬) R) ∪ ((¬) S)).
Proof.
  intros;destruct_eqRel.
  -destruct H.
   apply NNPP.
   unfold not;intros.
   apply H0.
   split.
   +apply NNPP.
    unfold not;intros.
    apply H1.
    left;constructor;assumption.
   +apply NNPP.
    unfold not;intros.
    apply H1.
    right;constructor;assumption.
  -destruct H.
   +destruct H.
    constructor.
    *assumption.
    *unfold not;intros.
     apply H0;destruct H1;assumption.
   +destruct H.
    constructor.
    *assumption.
    *unfold not;intros.
     apply H0;destruct H1;assumption.
Qed.

(*Primera ley del tercer excluido.*)
Lemma NEG_EXMDL_1: ∀ (A:Type) (R:relation A),
  coref R → (R ∪ ((¬) R)) = id.
Proof.
  intros;destruct_eqRel.
  -destruct H0.
   +apply H;assumption.
   +destruct H0;assumption.
  -apply NNPP.
   unfold not;intros.
   assert (((((¬) R) ∩ ((¬) ((¬) R)))) x y).
   +split.
    *constructor.
     --assumption.
     --unfold not;intros;apply H1.
       left;assumption.
    *constructor.
     --assumption.
     --unfold not;intros;apply H1.
       right;assumption.
   +destruct H2.
    rewrite NEG_DBLNEG in H3.
    *destruct H2.
     contradiction.
    *assumption.
Qed.

(*Segunda ley del tercer excluido.*)
Lemma NEG_EXMDL_2:∀ (A:Type) (R:relation A),
  (R ∩ ((¬) R)) = (@vacia A).
Proof.
  intros;destruct_eqRel.
  -destruct H.
   destruct H0.
   contradiction.
  -contradiction.
Qed.

(*
 * Ecuación para la negación del dominio de la 
 * composición de una relación inyectva y simple 
 * con cualquier otra.
 *)
Lemma NEG_DOM: ∀ (A:Type) (R S:relation A),
    iny S → simple S →
    ((¬) dom (R <∘> S))
    =
    (((¬) dom S) ∪ ((S ~) <∘> ((¬) dom R) <∘> S)).
Proof.
  intros;apply eqRelEq;split.
  -apply GC_DIFF.
   rewrite dAso.
   rewrite dC with (dom (R <∘> S)) ((¬) dom S).
   rewrite <- dAso.
   apply GC_DIFF.
   change (id \ ((¬) dom S)) with ((¬) ((¬) dom S)).
   rewrite NEG_DBLNEG.
   +rewrite DOM_COMP.
    *unfold inclusion;intros.
     rewrite <- sumProd.
     rewrite <- distr1.
     destruct H1.
     do 2 destruct H2.
     exists x0;split.
     --exists x0;split.
       ++assumption.
       ++rewrite NEG_EXMDL_1.
         **reflexivity.
         **apply domCoref.
     --assumption.
    *assumption.
   +apply domCoref.
  -unfold inclusion;intros.
   destruct H1.
   +destruct H1.
    constructor.
    *assumption.
    *unfold not;intros;apply H2.
     clear H1 H2.
     rewrite DOM_COMP in H3.
     --destruct H3;destruct H1.
       do 2 destruct H1.
       apply domCoref in H3.
       rewrite H3 in H1;clear H3.
       split.
       ++apply H.
         exists x0;split;assumption.
       ++exists x0;split;assumption.
     --assumption.
   +rewrite <- NEG_DBLNEG with A
                               (((S ~) <∘> ((¬) dom R)) <∘> S)
       in H1.
    *revert H1.
     revert x y.
     apply GC_DIFF.
     rewrite DOM_COMP.
     --rewrite <- NEG_DEMORGAN_2.
       rewrite <- NEG_CMPL_1.
       apply antimono.
       ++unfold coref;unfold inclusion;intros;destruct H1;assumption.
       ++rewrite NEG_DBLNEG.
         **rewrite NEG_DBLNEG.
           ---unfold inclusion;intros.
              apply lM in H1.
              do 2 destruct H1.
              rewrite <- invI with
                  (((S ~) <∘> ((¬) dom R))
                     ∩ ((((S ~) <∘> dom R) <∘> S) <∘> (S ~))) in H1.
              rewrite invProd in H1.
              rewrite invComp with (S ~) ((¬) dom R) in H1.
              assert ((((((¬) dom R) ~) <∘> ((S ~) ~))
                         ∩ (((((S ~) <∘> dom R) <∘> S)
                               <∘> (S ~)) ~)) x0 x).
              assumption.
              apply lM in H3.
              clear H1.
              destruct H3;destruct H1.
              rewrite invI in H3.
              assert ((((((¬) dom R) ~)
                          ∩ ((((((S ~)
                                   <∘> dom R) <∘> S)
                                 <∘> (S ~)) ~) <∘> (((S ~) ~) ~)))~)
                        x1 x0).
              assumption.
              clear H1.
              rewrite invProd in H4.
              destruct H4.
              rewrite invI in H4.
              rewrite invComp in H4.
              rewrite invI in H4.
              rewrite invComp in H4.
              rewrite invI in H4.
              rewrite invComp in H4.
              rewrite invI in H4.
              rewrite invI in H1.
              assert ((dom R) x0 y0).
              +++clear H2 H3 H1.
                 destruct H4;destruct H1.
                 do 6 destruct H2.
                 assert (id x0 x4).
                 apply H0.
                 exists x1;split;assumption.
                 clear H1 H2.
                 assert (id x3 y0).
                 apply H0.
                 exists x2;split;assumption.
                 clear H4 H3.
                 rewrite H6.
                 rewrite <- H1.
                 assumption.
              +++destruct H1.
                 contradiction.              
           ---unfold coref;unfold inclusion;intros;inversion H1.
         **unfold coref;unfold inclusion;intros.
           destruct H1.
           do 4 destruct H2.
           apply domCoref in H4.
           rewrite H4 in H2.
           apply H.
           exists x0;split;assumption.
     --assumption.
    *unfold coref;unfold inclusion;intros.
     do 4 destruct H2.
     destruct H4.
     rewrite H4 in H2.
     apply H.
     exists y1;split;assumption.
Qed.

(*
 * Ecuación para la negación del dominio de la 
 * composición de una relación cualquiera con
 * una correflexiva.
 *)
Theorem NEG_DOMCOREFL: ∀ (A:Type) (R S:relation A),
  coref S →
  ((¬) dom (R <∘> S)) = (((¬) S) ∪ (((¬) dom R) <∘> S)).
Proof.
  intros.
  rewrite NEG_DOM.
  -destruct_eqRel.
   +destruct H0.
    *destruct H0.
     left.
     constructor.
     --assumption.
     --unfold not;intros;apply H1.
       split.
       ++assumption.
       ++exists x;split.
         **rewrite <- H0 in H2.
           assumption.
         **assumption.
    *rewrite <- opA in H0.
     do 2 destruct H0.
     right.
     apply H in H0.
     rewrite H0 in H1.
     assumption.
   +destruct H0.
    *left.
     destruct H0.
     constructor.
     --assumption.
     --unfold not;intros;apply H1.
       destruct H2.
       do 2 destruct H3.
       apply H in H3.
       rewrite H3 in H4.
       assumption.
    *right.
     do 2 destruct H0.
     exists x0;split.
     --exists x;split.
       ++destruct H0.
         assert (id y0 y).
         **apply H;assumption.
         **rewrite <- H3 in H1.
           rewrite <- H0 in H1.
           assumption.
       ++assumption.
     --assumption.
  -unfold iny;unfold inclusion;intros.
   do 2 destruct H0.
   apply H in H0.
   rewrite H0 in H1.
   apply H.
   assumption.
  -unfold simple;unfold inclusion;intros.
   do 2 destruct H0.
   apply H in H1.
   rewrite <- H1 in H0.
   apply H.
   assumption.
Qed.

(*
 * Cerradura reflexiva-transitiva de R respecto a una relación S
 * utilizando el mayor punto fijo.
 *)
CoInductive coCD {A:Type} (R S:relation A): relation A :=
| cod (x y:A): S x y → coCD R S x y
| cotd (x y z:A): R x y → coCD R S y z → coCD R S x z.

(*
 * Cerradura de secuencias, es decir, 
 * la transición que va al infinito.
 *)
CoInductive infiStar {A:Type} (R:relation A): A → Prop :=
| infiIntro (x y:A): R x y → infiStar R y → infiStar R x.

(*Se cumple el caso inductivo del teorema de Knaster-Tarski para coCD.*)
Proposition nuind: ∀ (A:Type) (R S T:relation A),
    T ⊆ (S ∪ (R <∘> T)) ->
    T ⊆ (coCD R S).
Proof.
  cofix COINDHYP.
  intros.
  unfold inclusion;intros.
  apply H in H0.
  destruct H0.
  -constructor 1.
   assumption.
  -do 2 destruct H0.
   constructor 2 with x0.
   +assumption.
   +revert H1.
    apply COINDHYP.
    assumption.
Qed.

(*
 * La cerradura reflexiva-transitiva finita está 
 * contenida en la infinita.
 *)
Lemma crtCD_cont_coCD: ∀ (A:Type) (R S:relation A),
  (cd R S) ⊆ (coCD R S).
Proof.
  intros;unfold inclusion;intros.
  induction H.
  -constructor 1;assumption.
  -constructor 2 with y;assumption.
Qed.

(*
 * Si dados x,y se cumplen en la cerradura reflexiva-transitiva 
 * infinita pero no en la finita, entonces estos 
 * pertenecen a la relación de secuencias de R.
 *)
Lemma coCD_noCRT_infi: ∀ (A:Type) (R S:relation A) (x y:A),
    ((coCD R S) ∩ (compl (cd R S))) x y → (infiStar R x).
Proof.
  cofix COINDHYP;intros.
  do 2 destruct H.
  -exfalso.
   apply H0.
   constructor 1;assumption.
  -constructor 1 with y.
   +assumption.
   +apply COINDHYP with S z.
    split.
    *assumption.
    *unfold compl;unfold not;intros.
     apply H0.
     constructor 2 with y;assumption.
Qed.

(*La relación con la que se compone está contenida en la infinita.*)
Remark S_cont_coCD: ∀ (A:Type) (R S:relation A),
  S ⊆ (coCD R S).
Proof.
  unfold inclusion;intros.
  constructor 1;assumption.
Qed.

(*
 * Dados x,y si están en la relación de secuencias, 
 * están en la cerradura reflexiva-transitiva infinita.
 *)
Lemma infiS_cont_coCD: ∀ (A:Type) (R S:relation A) (x y:A),
    (infiStar R x) → (coCD R S) x y.
Proof.
  cofix COINDHYP;intros.
  destruct H.
  constructor 2 with y0.
  -assumption.
  -apply COINDHYP.
   assumption.
Qed.

(*
 * La cerradura reflexiva-transitiva infinita es equivalente a la
 * union de la reflexiva-transitiva finita o bien la de secuencias.
 *)
Theorem coCD_eq_or: ∀ (A:Type) (R S:relation A) (x y:A),
    coCD R S x y ↔ (((cd R S) x y) ∨ (infiStar R x) ∨ (S x y)).
Proof.
  split.
  -intros.
   destruct (classic (((cd R S) x y))).
   +left;assumption.
   +right.
    left.
    apply coCD_noCRT_infi with S y.
    split;assumption.
  -intros;destruct H.
   +apply crtCD_cont_coCD;assumption.
   +destruct H.
    *apply infiS_cont_coCD;assumption.
    *apply S_cont_coCD;assumption.
Qed.

(*
 * Si no hay posibilidad de ciclos infinitos, entonces
 * las cerraduras reflexivas-transitivas, finita e infinita,
 * son equivalentes.
 *)
Corollary CLOSURE_UEP_2: ∀ (A:Type) (R S:relation A),
    (∀ (x:A),¬infiStar R x) → cd R S = coCD R S.
Proof.
  intros;destruct_eqRel.
  -induction H0.
   +constructor 1;assumption.
   +constructor 2 with y;assumption.
  -apply coCD_eq_or in H0.
   destruct H0.
   +assumption.
   +destruct H0.
    *apply H in H0.
     contradiction.
    *constructor 1;assumption.
Qed.