(*
* Propiedades básicas del zipper relacional.
*
* Desarrollado por Fernando Abigail Galicia Mendoza
* Contacto: fernandogamen@ciencias.unam.mx
* Coq v8.7
* OCaml v4.05.0
*
* Trabajo desarrollado bajo el marco del proyecto:
* "Tópicos en computación teórica" (PAPIME, PE102117)
* Otorgado por la DGAPA de la UNAM
*
* Referencias:
* 1. Ikeda y Nishimura:
*    "Calculating Tree Navigation with symmetric relational zipper."
*)

Require Export Lib.Zipper.Zipper_Rel.

Ltac destruct_coref :=
  match goal with
  | [ |- ∀ A, coref ?B ] =>
    intro;unfold coref;unfold inclusion;intros
  end.

Ltac prueba_coref :=
  match goal with
  | [ H : ?P |- id ?x ?y ] => destruct H;rewrite H;
                              reflexivity
  end.

(*
 * Las relaciones correflexivas inducidas por un conjunto,
 * son correflexivas.
*)

Remark nodeCoref_coref: ∀ (A:Type), coref (@nodeCoref A).
Proof.
  destruct_coref;prueba_coref.
Qed.

Remark leftCoref_coref: ∀ (A:Type), coref (@leftCoref A).
Proof.
  destruct_coref;prueba_coref.
Qed.

Remark rightCoref_coref: ∀ (A:Type), coref (@rightCoref A).
Proof.
  destruct_coref;prueba_coref.
Qed.

Remark topCoref_coref: ∀ (A:Type), coref (@topCoref A).
Proof.
  destruct_coref;destruct H;reflexivity.
Qed.

Remark emptyCoref_coref: ∀ (A:Type), coref (@emptyCoref A).
Proof.
  destruct_coref;destruct H;reflexivity.
Qed.

Remark betaCoref_coref: ∀ (A:Type) (a:A),
    coref (betaCoref a).
Proof.
  unfold coref;unfold inclusion;intros;destruct H.
  rewrite H;reflexivity.
Qed.
  
(*La inversa de Dn es Up*)
Theorem Dn_Up_inv: ∀ (A:Type),
    (Dn A)~ = Up A.
Proof.
  unfold inv;intros;destruct_eqRel.
  -destruct H;constructor.
  -destruct H;constructor.
Qed.

(*La inversa de Up es Dn*)
Theorem Up_Dn_inv: ∀ (A:Type),
    (Up A)~ = Dn A.
Proof.
  intros.
  rewrite <- invI with (Dn A).
  rewrite Dn_Up_inv;reflexivity.
Qed.

(*La composición de Dn y Up está contenida en la relación identidad.*)
Lemma Up_Dn_Incid: ∀ (A:Type),
    (Up A <∘> Dn A) ⊆ id.
Proof.
  intros;unfold inclusion;intros.
  do 3 destruct H.
  -inversion H0;reflexivity.
  -inversion H0;reflexivity.
Qed.

(*
* La composición de Dn y Up es lo mismo que el producto
* de la relación inducida por el conjunto de nodos y la identidad.
*)
Proposition Up_Dn_nodocrId: ∀ (A:Type),
    (Up A <∘> Dn A) = (nodeCoref <X> id).
Proof.
  intros;destruct_eqRel.
  -do 3 destruct H.
   +inversion H0;constructor.
    *constructor;reflexivity.
    *reflexivity.
   +inversion H0;constructor.
    *constructor;reflexivity.
    *reflexivity.
  -do 2 destruct H.
   exists (l1,L x r1::w);split.
   +constructor.
   +inversion H.
    rewrite H0;constructor.
Qed.

(*
* El producto de la relación identidad y la relación inversa inducida
* por las migajas izquierdas es lo mismo que el producto que la relación
* identidad y la relación inversa inducida por las migajas izquierdas.
*)
Remark leftCoref_id: ∀ (A:Type),
    ((@produ (tree A) (ctx A) id leftCoref)~) =
    (id <X> leftCoref).
Proof.
  intros;destruct_eqRel.
  -destruct H;constructor.
   +symmetry;assumption.
   +destruct H0.
    constructor;symmetry;assumption.
  -destruct H;constructor.
   +symmetry;assumption.
   +destruct H0.
    constructor;symmetry;assumption.
Qed.

(*
* El producto de la relación identidad y la relación inversa inducida
* por las migajas derechas es lo mismo que el producto que la relación
* identidad y la relación inversa inducida por las migajas derechas.
*)
Remark rightCoref_id: ∀ (A:Type),
    ((@produ (tree A) (ctx A) id rightCoref)~) =
    (id <X> rightCoref).
Proof.
  intros;destruct_eqRel.
  -destruct H;constructor.
   +symmetry;assumption.
   +destruct H0.
    constructor;symmetry;assumption.
  -destruct H;constructor.
   +symmetry;assumption.
   +destruct H0.
    constructor;symmetry;assumption.
Qed.

(*La inversa de dn_L es up_L.*)
Corollary UPDN_INVERSE_1 : ∀ (A:Type),
    (@dn_L A ~) = up_L.
Proof.
  intros.
  unfold dn_L.
  rewrite invComp.
  rewrite Dn_Up_inv.
  unfold up_L.
  destruct_eqRel.
  -rewrite <- leftCoref_id;assumption.
  -rewrite <- leftCoref_id;assumption.
Qed.

(*La inversa de dn_R es up_R.*)
Corollary UPDN_INVERSE_2 : ∀ (A:Type),
    (@dn_R A ~) = up_R.
Proof.
  intros.
  unfold dn_R.
  rewrite invComp.
  rewrite Dn_Up_inv.
  unfold up_R.
  destruct_eqRel.
  -rewrite <- rightCoref_id in H;assumption.
  -rewrite <- rightCoref_id;assumption.
Qed.

(*La inversa de up_L es dn_L.*)
Corollary UPDN_INVERSE_3 : ∀ (A:Type),
    (@up_L A~) = dn_L.
Proof.
  intros;rewrite <- UPDN_INVERSE_1;apply invI.
Qed.

(*La inversa de up_R es dn_R.*)
Corollary UPDN_INVERSE_4 : ∀ (A:Type),
    (@up_R A~) = dn_R.
Proof.
  intros;rewrite <- UPDN_INVERSE_2;apply invI.
Qed.

(*up_L es simple*)
Theorem DnUp_COREFL_1: ∀ (A:Type),
    simple (@up_L A).
Proof.
  intros;unfold simple.
  rewrite UPDN_INVERSE_3.
  unfold inclusion;intros.
  do 2 destruct H.
  do 2 destruct H.
  do 2 destruct H0.
  destruct H1.
  apply leftCoref_coref in H3.
  rewrite <- H1 in H0.
  rewrite <- H3 in H0.
  destruct H0.
  rewrite H0 in H.
  apply leftCoref_coref in H4.
  rewrite H4 in H.
  apply Up_Dn_Incid.
  exists (y1,z0);split;assumption.
Qed.

(*up_R es simple*)
Theorem DnUp_COREFL_2: ∀ (A:Type),
    simple (@up_R A).
Proof.
  intros;unfold simple.
  rewrite UPDN_INVERSE_4.
  unfold inclusion;intros.
  do 2 destruct H.
  do 2 destruct H.
  do 2 destruct H0.
  destruct H1.
  apply rightCoref_coref in H3.
  rewrite <- H1 in H0.
  rewrite <- H3 in H0.
  destruct H0.
  rewrite H0 in H.
  apply rightCoref_coref in H4.
  rewrite H4 in H.
  apply Up_Dn_Incid.
  exists (y1,z0);split;assumption.
Qed.  
  
(*dn_L es simple*)
Theorem DnUp_COREFL_3: ∀ (A:Type),
    simple (@dn_L A).
Proof.
  unfold simple;unfold inclusion;intros.
  rewrite UPDN_INVERSE_1 in H.
  do 4 destruct H;do 2 destruct H0.
  destruct H.
  rewrite <- H in H1;clear H.
  destruct H3.
  rewrite <- H in H1;clear H.
  destruct H2.
  rewrite H in H0;clear H.
  destruct H2.
  rewrite H in H0;clear H.
  destruct x0.
  destruct t.
  -inversion H1.
  -inversion H1.
   inversion H0;reflexivity.
Qed.

(*dn_R es simple*)
Theorem DnUp_COREFL_4: ∀ (A:Type),
    simple (@dn_R A).
Proof.
  unfold simple;unfold inclusion;intros.
  rewrite UPDN_INVERSE_2 in H.
  do 4 destruct H;do 2 destruct H0.
  destruct H.
  rewrite <- H in H1;clear H.
  destruct H3.
  rewrite <- H in H1;clear H.
  destruct H2.
  rewrite H in H0;clear H.
  destruct H2.
  rewrite H in H0;clear H.
  destruct x0.
  destruct t.
  -inversion H1.
  -inversion H1.
   inversion H0;reflexivity.
Qed.

(*La composición de up_L y dn_R es vacía*)
Lemma UpDn_Emp_1: ∀ (A:Type),
    ((@up_L A) <∘> dn_R) = vacia.
Proof.
  intros;apply eqRelEq;split;unfold inclusion;intros.
  -do 4 destruct H;do 2 destruct H0.
   destruct H;do 2 destruct H1;inversion H0;inversion H7.
  -inversion H.
Qed.

(*La composición de up_R y dn_L es vacía*)
Lemma UpDn_Emp_2: ∀ (A:Type),
    ((@up_R A) <∘> dn_L) = vacia.
Proof.
  intros;destruct_eqRel.
  -do 4 destruct H;do 2 destruct H0.
   destruct H;do 2 destruct H1;inversion H0;inversion H7.
  -inversion H.
Qed.

Lemma DnR_UpL_NEmp: ∀ (A:Type) (i:A) (l r:tree A) (c:ctx A),
    ((@dn_R A)  <∘> up_L) <> vacia.
Proof.
  unfold not;intros.
  apply eqRelEq in H;unfold same_relation in H;destruct H.
  destruct H with (r,R i l::c) (l,L i r::c).
  exists (Node l i r,c);split.
  unfold dn_R.
  exists (r,R i l ::c);split.
  constructor.
  reflexivity.
  constructor.
  reflexivity.
  constructor.
  unfold up_L.
  exists (l,L i r::c);split.
  constructor.
  constructor.
  reflexivity.
  constructor.
  reflexivity.
Qed.

Remark L_T_Emp: ∀ (A:Type),
    ((@leftCoref A) <∘> topCoref) = vacia.
Proof.
  intros;destruct_eqRel.
  -do 2 destruct H.
   destruct H.
   inversion H0.
  -inversion H.
Qed.
  

(*La composición de up_L con el producto de
la relación identidad y Top? es vacía.*)
Theorem Up_Emp_1: ∀ (A:Type),
    ((@up_L A)  <∘> (id <X> topCoref)) = vacia.
Proof.
  intros;destruct_eqRel.
  -do 4 destruct H;destruct H1.
   destruct H2.
   inversion H0.
   inversion H7.
  -inversion H.
Qed.

(*La composición de up_R con el producto de
la relación identidad y Top? es vacía.*)
Theorem Up_Emp_2: ∀ (A:Type),
    ((@up_R A)  <∘> (id <X> topCoref)) = vacia.
Proof.
  intros;destruct_eqRel.
  -do 4 destruct H.
   do 2 destruct H1.
   destruct H2.
   inversion H0.
   inversion H6.
  -inversion H.
Qed.

(*La composición de dn_L con el producto de
la relación identidad y Leaf? es vacía.*)
Theorem Dn_Emp_1: ∀ (A:Type),
    ((@dn_L A)  <∘> (emptyCoref <X> id)) = vacia.
Proof.
  intros;destruct_eqRel.
  -do 4 destruct H.
   destruct H.
   destruct H2.
   inversion H1.
   rewrite <- H3 in H0.
   inversion H0.
   inversion H10.
  -inversion H.
Qed.

(*La composición de dn_R con el producto de
la relación identidad y Leaf? es vacía.*)
Theorem Dn_Emp_2: ∀ (A:Type),
    ((@dn_R A)  <∘> (emptyCoref <X> id)) = vacia.
Proof.
  intros;destruct_eqRel.
  -do 5 destruct H.
   inversion H2.
   rewrite <- H5 in H1.
   inversion H1.
   rewrite <- H6 in H0.
   inversion H0.
   inversion H13.
  -inversion H.
Qed.

(*
* Para cualesquiera relaciones T,S, se tiene que:
* Si S está contenida en T compueta con dn_L, entonces
* S compuesta con la inversa de dn_L está contenida
* en T.
*)
Lemma UpDn_Shunt_1: ∀ (A:Type) (S T:relation (zipper A)),
    (S ⊆ (T <∘> (@dn_L A))) → ((S <∘> (dn_L ~)) ⊆ T).
Proof.
  unfold inclusion;intros.
  do 2 destruct H0.
  apply H in H0.
  do 2 destruct H0.
  assert ((dn_L  <∘> (dn_L ~)) x1 y).
  exists x0;split;assumption.
  clear H2 H1;apply DnUp_COREFL_3 in H3.
  rewrite <- H3;assumption.
Qed.

(*
* Para cualesquiera relaciones T,S, se tiene que:
* Si S está contenida en T compueta con dn_L, entonces
* S compuesta con la inversa de dn_R está contenida
* en T.
*)
Lemma UpDn_Shunt_2: ∀ (A:Type) (S T:relation (zipper A)),
    (S ⊆ (T <∘> (@dn_R A))) → ((S <∘> (dn_R ~)) ⊆ T).
Proof.
  unfold inclusion;intros.
  do 2 destruct H0.
  apply H in H0.
  do 2 destruct H0.
  assert ((dn_R  <∘> (dn_R ~)) x1 y).
  -exists x0;split;assumption.
  -clear H2 H1;apply DnUp_COREFL_4 in H3.
   rewrite <- H3;assumption.
Qed.

(*
* Para cualesquiera relaciones T,S, se tiene que:
* Si S está contenida en T compueta con up_L, entonces
* S compuesta con la inversa de up_L está contenida
* en T.
*)
Lemma UpDn_Shunt_3: ∀ (A:Type) (S T:relation (zipper A)),
    (S ⊆ (T <∘> (@up_L A))) → ((S <∘> (up_L ~)) ⊆ T).
Proof.
  unfold inclusion;intros.
  do 2 destruct H0.
  apply H in H0.
  do 2 destruct H0.
  assert ((up_L <∘> (up_L ~)) x1 y).
  -exists x0;split;assumption.
  -clear H2 H1;apply DnUp_COREFL_1 in H3.
   rewrite <- H3;assumption.
Qed.

(*
* Para cualesquiera relaciones T,S, se tiene que:
* Si S está contenida en T compueta con up_R, entonces
* S compuesta con la inversa de up_R está contenida
* en T.
*)
Lemma UpDn_Shunt_4: ∀ (A:Type) (S T:relation (zipper A)),
    (S ⊆ (T <∘> (@up_R A))) → ((S <∘> (up_R ~)) ⊆ T).
Proof.
  unfold inclusion;intros.
  do 2 destruct H0.
  apply H in H0.
  do 2 destruct H0.
  assert ((up_R <∘> (up_R ~)) x1 y).
  -exists x0;split;assumption.
  -clear H2 H1;apply DnUp_COREFL_2 in H3.
   rewrite <- H3;assumption.
Qed.

(*
* Para cualesquiera relaciones T,S, se tiene que:
* Si S está contenida en dn_L compuesta con T, entonces
* la inversa de dn_L está compuesta con S está contenida
* en T.
*)
Lemma UpDn_Shunt_5: ∀ (A:Type) (S T:relation (zipper A)),
    (S ⊆ ((@dn_L A) <∘> T)) → (((dn_L ~) <∘> S) ⊆ T).
Proof.
  unfold inclusion;intros.
  do 2 destruct H0.
  apply H in H1.
  do 2 destruct H1.
  assert (((dn_L ~)  <∘> dn_L) x x1).
  -exists x0;split;assumption.
  -clear H0 H1.
   rewrite <- invI with dn_L in H3.
   rewrite UPDN_INVERSE_1 in H3.
   rewrite UPDN_INVERSE_1 in H3.
   apply DnUp_COREFL_1 in H3.
   rewrite H3;assumption.
Qed.

(*
* Para cualesquiera relaciones T,S, se tiene que:
* Si S está contenida en dn_R compuesta con T, entonces
* la inversa de dn_R está compuesta con S está contenida
* en T.
*)
Lemma UpDn_Shunt_6: ∀ (A:Type) (S T:relation (zipper A)),
    (S ⊆ ((@dn_R A) <∘> T)) → (((dn_R ~) <∘> S) ⊆ T).
Proof.
  unfold inclusion;intros.
  do 2 destruct H0.
  apply H in H1.
  do 2 destruct H1.
  assert (((dn_R ~)  <∘> dn_R) x x1).
  -exists x0;split;assumption.
  -clear H0 H1.
   rewrite <- invI with dn_R in H3.
   rewrite UPDN_INVERSE_2 in H3.
   rewrite UPDN_INVERSE_2 in H3.
   apply DnUp_COREFL_2 in H3.
   rewrite H3;assumption.
Qed.

(*
* Para cualesquiera relaciones T,S, se tiene que:
* Si S está contenida en up_L compuesta con T, entonces
* la inversa de up_L está compuesta con S está contenida
* en T.
*)
Lemma UpDn_Shunt_7: ∀ (A:Type) (S T:relation (zipper A)),
    (S ⊆ ((@up_L A) <∘> T)) → (((up_L ~) <∘> S) ⊆ T).
Proof.
  unfold inclusion;intros.
  do 2 destruct H0.
  apply H in H1.
  do 2 destruct H1.
  assert (((up_L ~)  <∘> up_L) x x1).
  -exists x0;split;assumption.
  -clear H0 H1.
   rewrite <- UPDN_INVERSE_1 in H3.
   rewrite invI in H3.
   apply DnUp_COREFL_3 in H3.
   rewrite H3;assumption.
Qed.

(*
* Para cualesquiera relaciones T,S, se tiene que:
* Si S está contenida en up_R compuesta con T, entonces
* la inversa de up_R está compuesta con S está contenida
* en T.
*)
Lemma UpDn_Shunt_8: ∀ (A:Type) (S T:relation (zipper A)),
    (S ⊆ ((@up_R A) <∘> T)) → (((up_R ~) <∘> S) ⊆ T).
Proof.
  unfold inclusion;intros.
  do 2 destruct H0.
  apply H in H1.
  do 2 destruct H1.
  assert (((up_R ~)  <∘> up_R) x x1).
  -exists x0;split;assumption.
  -clear H0 H1.
   rewrite <- UPDN_INVERSE_2 in H3.
   rewrite invI in H3.
   apply DnUp_COREFL_4 in H3.
   rewrite H3;assumption.
Qed.

(* dn_L = dn_L <∘> (dn_L ~) <∘> dn_L *)
Theorem UpDn_Triple_1: ∀ (A:Type),
    (@dn_L A) = (dn_L <∘> (dn_L ~) <∘> dn_L).
Proof.
  intros;destruct_eqRel.
  -rewrite <- opA.
   exists y;split.
   +assumption.
   +destruct y;destruct t.
    *do 2 destruct H.
     inversion H0.
    *exists (t1,L a t2::c);split.
     --rewrite UPDN_INVERSE_1;unfold up_L.
       exists (t1,L a t2::c);split.
       ++constructor.
       ++constructor.
         **reflexivity.
         **constructor;reflexivity.
     --unfold dn_L.
       exists (t1,L a t2::c);split.
       ++constructor.
         **reflexivity.
         **constructor;reflexivity.
       ++constructor.
  -do 2 destruct H.
   apply DnUp_COREFL_3 in H.
   rewrite H;assumption.
Qed.

(* dn_R = dn_R <∘> (dn_R ~) <∘> dn_R *)
Theorem UpDn_Triple_2: ∀ (A:Type),
    (@dn_R A) = (dn_R <∘> (dn_R ~) <∘> dn_R).
Proof.
  intros;destruct_eqRel.
  -rewrite <- opA.
   exists y;split.
   +assumption.
   +destruct y;destruct t.
    *do 2 destruct H.
     inversion H0.
    *exists (t2,R a t1::c);split.
     --rewrite UPDN_INVERSE_2;unfold up_L.
       exists (t2,R a t1::c);split.
       ++constructor.
       ++constructor.
         **reflexivity.
         **constructor;reflexivity.
     --unfold dn_R.
       exists (t2,R a t1::c);split.
       ++constructor.
         **reflexivity.
         **constructor;reflexivity.
       ++constructor.
  -destruct H;destruct H.
   apply DnUp_COREFL_4 in H.
   rewrite H;assumption.
Qed.

(* up_L = up_L <∘> (up_L ~) <∘> up_L *)
Theorem DnUp_Triple_3: ∀ (A:Type),
    (@up_L A) = (up_L <∘> (up_L ~) <∘> up_L).
Proof.
  intros;destruct_eqRel.
  -exists x;split.
   +destruct x;destruct t.
    *do 2 destruct H.
     inversion H.
    *exists (t1,L a t2::c);split.
     --unfold up_L.
       exists (t1,L a t2::c);split.
       ++constructor.
       ++constructor.
         **reflexivity.
         **constructor;reflexivity.
     --rewrite UPDN_INVERSE_3;unfold dn_L.
       exists (t1,L a t2::c);split.
       ++constructor.
         **reflexivity.
         **constructor;reflexivity.
       ++constructor.
   +assumption.
  -destruct H;destruct H.
   apply DnUp_COREFL_1 in H.
   rewrite H;assumption.
Qed.

(* up_R = up_R <∘> (up_R ~) <∘> up_R *)
Theorem DnUp_Triple_4: ∀ (A:Type),
    (@up_R A) = (up_R <∘> (up_R ~) <∘> up_R).
Proof.
  intros;destruct_eqRel.
  -exists x;split.
   +destruct x;destruct t.
    *do 2 destruct H.
     inversion H.
    *exists (t2,R a t1::c);split.
     --unfold up_R.
       exists (t2,R a t1::c);split.
       ++constructor.
       ++constructor.
         **reflexivity.
         **constructor;reflexivity.
     --rewrite UPDN_INVERSE_4;unfold dn_R.
       exists (t2,R a t1::c);split.
       ++constructor.
         **reflexivity.
         **constructor;reflexivity.
       ++constructor.
   +assumption.
  -destruct H;destruct H.
   apply DnUp_COREFL_2 in H.
   rewrite H;assumption.
Qed.

(*La composición de la inversa de up_L y up_L es correflexiva.*)
Remark uplinv_upl_coref: ∀ (A:Type),
    coref ((@up_L A ~) <∘> up_L).
Proof.
  unfold coref;unfold inclusion;intros.
  apply DnUp_COREFL_3.
  rewrite <- invI with dn_L.
  rewrite UPDN_INVERSE_1.
  rewrite invI.
  assumption.
Qed.

(*La composición de la inversa de up_R y up_R es correflexiva.*)
Remark uprinv_upr_coref: ∀ (A:Type),
    coref ((@up_R A ~) <∘> up_R).
Proof.
  unfold coref;unfold inclusion;intros.
  apply DnUp_COREFL_4.
  rewrite <- invI with dn_R.
  rewrite UPDN_INVERSE_2.
  rewrite invI.
  assumption.
Qed.

(*La composición de la inversa de dn_L y dn_L es correflexiva.*)
Remark dnlinv_dnl_coref: ∀ (A:Type),
    coref ((@dn_L A ~) <∘> dn_L).
Proof.
  unfold coref;unfold inclusion;intros.
  apply DnUp_COREFL_1.
  rewrite <- UPDN_INVERSE_1.
  rewrite invI.
  assumption.
Qed.

(*La composición de la inversa de dn_L y dn_L es correflexiva.*)
Remark dnrinv_dnr_coref: ∀ (A:Type),
    coref ((@dn_R A ~) <∘> dn_R).
Proof.
  unfold coref;unfold inclusion;intros.
  apply DnUp_COREFL_2.
  rewrite <- UPDN_INVERSE_2.
  rewrite invI.
  assumption.
Qed.

(*up_L es inyectiva.*)
Remark upl_iny: ∀ (A:Type),
    iny (@up_L A).
Proof.
  unfold iny;unfold inclusion;intros.
  apply uplinv_upl_coref;assumption.
Qed.

(*up_R es inyectiva.*)
Remark upr_iny: ∀ (A:Type),
    iny (@up_R A).
Proof.
  unfold iny;unfold inclusion;intros.
  apply uprinv_upr_coref;assumption.
Qed.