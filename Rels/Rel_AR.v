(*
* El conjunto de relaciones es un
* álgebra relacional.
*
* Desarrollado por Fernando Abigail Galicia Mendoza
* Contacto: fernandogamen@ciencias.unam.mx
* Coq v8.7
* OCaml v4.05.0
*
* Trabajo desarrollado bajo el marco del proyecto:
* "Tópicos en computación teórica" (PAPIME, PE102117)
* Otorgado por la DGAPA de la UNAM
*
* Referencias:
* 1. Ikeda & Nishimura:
*    "Calculating tree navigation with symmetric relational zipper"
*)

Require Export Lib.AlgRel.AR.
Require Export Lib.Rels.Rel_Def.
Require Import Program.

(*
* Necesitamos del axioma del
* tercero excluido.
*)
Require Export Classical.

(*Se abrevia la doble contención*)
Ltac destruct_eqRel :=
  match goal with
  | [ |- ?R = ?S ] => apply eqRelEq;
                      unfold same_relation;
                      split;unfold inclusion;intros
  end.

(*
* Demostremos que el conjunto de relaciones es un monoide con
* la identidad y el operador de composición
*)
Program Instance RMon : ∀ (A:Type),
    MonS (relation A) id comp.
Next Obligation with eauto.
  destruct_eqRel.
  -destruct H;destruct H.
   unfold id in H0;rewrite <- H0...
  -exists y;split;unfold id...
Qed.
Next Obligation with eauto.
  destruct_eqRel.
  -destruct H;destruct H;unfold id in H;rewrite H...
  -exists x0;unfold id;split...
Qed.
Next Obligation with eauto.
  destruct_eqRel.
  -destruct H;destruct H;destruct H0;destruct H0.
   exists x2;split...
   exists x1;split...
  -destruct H;destruct H;destruct H;destruct H.
   exists x2;split...
   exists x1;split...
Qed.

Hint Resolve RMon.

(*
* Demostremos que el conjunto de relaciones es un
* un álgebra de Bool con las relaciones total y vacía,
* el complemento, la union y la intersección.
*)
Program Instance RAB : ∀ (A:Type),
    AlgBool (relation A) total vacia compl union inter.
Next Obligation with eauto.
  destruct_eqRel.
  -destruct H...
   destruct H.
  -left...
Qed.
Next Obligation with eauto.
  destruct_eqRel.
  -destruct H...
  -destruct H...
Qed.
Next Obligation with eauto.
  destruct_eqRel.
  -destruct H...
  -split...
Qed.
Next Obligation with eauto.
  destruct_eqRel.
  -destruct H...
  -destruct H...
Qed.
Next Obligation with eauto.
  destruct_eqRel.
  -destruct H...
   destruct H...
  -destruct H...
   destruct H...
   destruct H0...
Qed.
Next Obligation with eauto.
  destruct_eqRel.
  -destruct H;destruct H0...
  -destruct H;destruct H...
Qed.
Next Obligation with eauto.
  destruct_eqRel.
  -apply t;assert (x x0 y ∨ (¬ x x0 y))...
  -assert (x x0 y ∨ (¬ x x0 y)).
   +apply classic.
   +destruct H0...
Qed.
Next Obligation.
  destruct_eqRel.
  -destruct H;contradiction.
  -destruct H.
Qed.

Hint Resolve RAB.

(*
* Demostremos que el conjunto de relaciones es un
* un álgebra relacional sinedo un monoide y álgebra de Bool
* como se demuestra arriba y las ecuaciones para ser álgebra
* relacional
*)
Program Instance RAR : ∀ (A:Type),
    AR (relation A) vacia total id compl inv
       union inter comp (RAB A) (RMon A).
Next Obligation with eauto.
  destruct_eqRel.
  -unfold inv,comp in H;destruct H;destruct H.
   unfold inv;exists x1;split...
  -unfold comp,inv in H;destruct H;destruct H.
   unfold inv;exists x1;split...
Qed.
Next Obligation with eauto.
  destruct_eqRel.
  -unfold comp in H;destruct H;destruct H;destruct H.
   +left;exists y1;split...
   +right;exists y1;split...
  -destruct H;unfold comp in H;destruct H;destruct H.
   +exists x1;split...
   +exists x1;split...
Qed.
Next Obligation with eauto.
  destruct_eqRel.
  -unfold inv in H;destruct H...
  -destruct H;unfold inv in H.
   +left...
   +right...
Qed.
Next Obligation with eauto.
  destruct_eqRel...
  destruct H...
  destruct H;destruct H;unfold compl in *;unfold not;intros;apply H0.
  unfold inv in H;exists x0;split...
Qed.

Hint Resolve RAR.