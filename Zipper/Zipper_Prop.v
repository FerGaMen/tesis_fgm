(*
* Propiedades sobre las funciones de navegación.
*
* Desarrollado por Fernando Abigail Galicia Mendoza
* Contacto: fernandogamen@ciencias.unam.mx
* Coq v8.7
* OCaml v4.05.0
*
* Trabajo desarrollado bajo el marco del proyecto:
* "Tópicos en computación teórica" (PAPIME, PE102117)
* Otorgado por la DGAPA de la UNAM
*
* Referencias:
* 1. Ikeda y Nishimura:
*    "Calculating tree navigation with symmetric relational zipper."
*)

Require Export Lib.Zipper.Zipper_Def.
Require Export Utf8.

(*La función dnL es parcial.*)
Remark dnL_parcial: ∀ (A:Type),
  ∃ (z:zipper A), dnL z = None.
Proof.
  intros.
  exists (Empty A,nil).
  reflexivity.
Qed.

(*La función dnR es parcial.*)
Remark dnR_parcial: ∀ (A:Type),
  ∃ (z:zipper A), dnR z = None.
Proof.
  intros.
  exists (Empty A,nil).
  reflexivity.
Qed.

(*La función upL es parcial.*)
Remark upL_parcial: ∀ (A:Type),
  ∃ (z:zipper A), upL z = None.
Proof.
  intros.
  exists (Empty A,nil).
  reflexivity.
Qed.

(*La función upR es parcial.*)
Remark upR_parcial: ∀ (A:Type),
  ∃ (z:zipper A), upR z = None.
Proof.
  intros.
  exists (Empty A,nil).
  reflexivity.
Qed.

(*
* Ikeda y Nishimura mencionan que la composición de las
* operaciones de navegación son una identidad parcial,
* para lograr esto es necesario utilizar el operador bind
* para la mónada option.
*)

(*Operador para mónada option*)
Definition bind {A B:Type} (a:option A) (f:A -> option B) : option B :=
  match a with
  | None => None
  | Some x => f x
  end.

Infix ">>=" := bind (at level 70).

(*Identidad parcial de componer dnL y upL.*)
Theorem dnL_upL_parcialId: ∀ (A:Type),
  ∃ (z:zipper A), (dnL z) >>= (@upL A) = None.
Proof.
  intros.
  exists (Empty A,nil).
  reflexivity.
Qed.

(*Identidad parcial de componer upL y dnL.*)
Theorem upL_dnL_parcialId: ∀ (A:Type),
  ∃ (z:zipper A), (upL z) >>= (@dnL A) = None.
Proof.
  intros.
  exists (Empty A,nil).
  reflexivity.
Qed.

(*Identidad parcial de componer dnR y upR.*)
Theorem dnR_upR_parcialId: ∀ (A:Type),
  ∃ (z:zipper A), (dnR z) >>= (@upR A) = None.
Proof.
  intros.
  exists (Empty A,nil).
  reflexivity.
Qed.

(*Identidad parcial de componer upR y dnR.*)
Theorem upR_dnR_parcialId: ∀ (A:Type),
  ∃ (z:zipper A), (upR z) >>= (@dnR A) = None.
Proof.
  intros.
  exists (Empty A,nil).
  reflexivity.
Qed.

(*La función go_up no es inyectiva*)
Theorem go_up_noIny: ∀ (A:Type) (x:A),
  ∃ (z1 z2:zipper A), go_up z1 = go_up z2 ∧ z1 ≠ z2.
Proof.
  intros.
  exists (Empty A,(L x (Empty A))::nil).
  exists (Empty A,(R x (Empty A))::nil).
  split.
  reflexivity.
  unfold not;intros.
  inversion H.
Qed.