(*
* Implementación de los monoides.
*
* Desarrollado por Fernando Abigail Galicia Mendoza
* Contacto: fernandogamen@ciencias.unam.mx
* Coq v8.7
* OCaml v4.05.0
*
* Trabajo desarrollado bajo el marco del proyecto:
* "Tópicos en computación teórica" (PAPIME, PE102117)
* Otorgado por la DGAPA de la UNAM
*
* 1. Castéran & Sozeau, en:
*    "A gentle introduction to type classes and relations in Coq"
*)

Require Export Utf8.

(*Definimos la estructura de monoide*)
Class MonS
      (A:Type)
      (e:A)
      (op:A → A → A)
  :={
      opND: ∀ (x:A),
        op x e = x;
      opNI: ∀ (x:A),
          op e x = x;
      opA: ∀ (x y z:A),
          op x (op y z) = op (op x y) z
    }.

Hint Resolve opND.
Hint Resolve opNI.
Hint Resolve opA.