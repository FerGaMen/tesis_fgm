(*
* Implementación del zipper relacional.
*
* Desarrollado por Fernando Abigail Galicia Mendoza
* Contacto: fernandogamen@ciencias.unam.mx
* Coq v8.7
* OCaml v4.05.0
*
* Trabajo desarrollado bajo el marco del proyecto:
* "Tópicos en computación teórica" (PAPIME, PE102117)
* Otorgado por la DGAPA de la UNAM
*
* Referencias:
* 1. Ikeda y Nishimura:
*    "Calculating tree navigation with symmetric relational zipper."
*)

Require Export Lib.Zipper.Zipper_Def.
Require Export Lib.Rels.Rel_Prop.

Local Notation " [ ] " := nil.
Local Notation " [ x ] " := (cons x nil).
Local Notation " [ x , y , .. , z ] " := (cons x (cons y .. (cons z nil) ..)).
Local Notation " ( x :: l ) " := (cons x l).

(*Traducción de la destrucción del árbol.*)
Inductive Dn (A:Type) : relation (zipper A) :=
| dnL (l r:tree A) (x:A) (c:ctx A):
    Dn A (l,L x r::c) (Node l x r,c) 
| dnR (l r:tree A) (x:A) (c:ctx A):
    Dn A (r,R x l::c) (Node l x r,c).

(*Traducción de la reconstrucción del árbol.*)
Inductive Up (A:Type) : relation (zipper A) :=
| upL (l r:tree A) (x:A) (c:ctx A):
    Up A (Node l x r,c) (l,L x r::c) 
| upR (l r:tree A) (x:A) (c:ctx A):
    Up A (Node l x r,c) (r,R x l::c).

(*Relación que contiene todos los nodos iguales.*)
Inductive nodeCoref {A:Type} : relation (tree A) :=
| ncr (x y:A) (l1 l2 r1 r2:tree A):
    Node l1 x r1 = Node l2 y r2 -> nodeCoref (Node l1 x r1) (Node l2 y r2).

Notation "t1 Node? t2" := (nodeCoref t1 t2) (at level 80).

(*
 * Relación que contiene todos las migajas 
 * cuya primera entrada sea un subárbol izquierdo.
*)
Inductive leftCoref {A:Type} : relation (ctx A) :=
| leftcr (x y:A) (r1 r2:tree A) (c1 c2:ctx A):
    (L x r1)::c1 = (L y r2)::c2 -> leftCoref ((L x r1)::c1) ((L y r2)::c2).

Notation "c1 L? c2" := (leftCoref c1 c2) (at level 80).

(*
 * Relación que contiene todos las migajas 
 * cuya primera entrada sea un subárbol derecho.
*)
Inductive rightCoref {A:Type} : relation (ctx A) :=
| rightcr (x y:A) (r1 r2:tree A) (c1 c2:ctx A):
    (R x r1)::c1 = (R y r2)::c2 -> rightCoref ((R x r1)::c1) ((R y r2)::c2).

Notation "c1 R? c2" := (rightCoref c1 c2) (at level 80).

(*Relación que contiene los contextos vacíos.*)
Inductive topCoref {A:Type} : relation (ctx A) :=
| nilcr: topCoref [] [].

Notation "c1 Top? c2" := (topCoref c1 c2) (at level 80).

(*Relación que contiene los árboles vacíos.*)
Inductive emptyCoref {A:Type} : relation (tree A) :=
| emcr: emptyCoref (Empty A) (Empty A).

Notation "c1 Leaf? c2" := (emptyCoref c1 c2) (at level 80).

(*Relación que contiene todos los nodos cuyas raíces sean las mismas.*)
Inductive betaCoref {A:Type} (b:A) : relation (zipper A) :=
| btI (i:A) (l r:tree A) (p:ctx A):
    b = i -> betaCoref b (Node l i r,p) (Node l b r,p).

Notation "z1 ( b ?) z2" := (betaCoref b z1 z2) (at level 80).

(*Traducción de la función dnL.*)
Definition dn_L {A:Type} : relation (zipper A) :=
  (id <X> (@leftCoref A)) <∘> (Dn A).

(*Traducción de la función dnR.*)
Definition dn_R {A:Type} : relation (zipper A) :=
  (id <X> (@rightCoref A)) <∘> (Dn A).

(*Traducción de la función upL.*)
Definition up_L {A:Type} : relation (zipper A) :=
  (Up A) <∘> (id <X> (@leftCoref A)).

(*Traducción de la función upR.*)
Definition up_R {A:Type} : relation (zipper A) :=
  (Up A) <∘> (id <X> (@rightCoref A)).