Proyecto: Verificación formal del zipper relacional simétrico

Autor: Fernando Abigail Galicia Mendoza
Correo electrónico: fernandogamen@ciencias.unam.mx

Desarrollado en: Coq (v8.7) compilado utilizando Ocaml en su version 4.05.0

Trabajo desarrollado en el marco del proyecto: "Tópicos en computación teórica" (PAPIME, PE102117)
Otorgado por la Dirección General de Asuntos del Personal Académico de la Universidad Nacional Autónoma de México.

---------------------------------------------------------------------------------------------------------------
Estructura del proyecto
---------------------------------------------------------------------------------------------------------------

Dada lo extenso de la teoría verificada formalmente, se optó por crear
distintos módulos que representen el análisis sobre cada teoría dada [1].

El proyecto está dividido en los siguientes módulos:
-AlgBool: Implementación de la estructura Álgebra de Boole.
-Monoide: Implementación de la estructura Monoide.
-AlgRel: Implementación de la estructura Álgebra Relacional.
-Rels: Implementación de definiciones, operadores y propiedades de relaciones binarias.
En este módulo se encuentran definidas las cerraduras inductivas y coinductivas.
-Zipper: Implementación del zipper que almacena árboles binarios, sus funciones de navegación
y traducción de estas últimas a relaciones binarias. Se muestran distintos resultados respecto
a estas relaciones binarias.
-XPath: Implementación del lenguaje XPath, su interpretación relacional y demostración de las
ecuaciones establecidas en [1].

---------------------------------------------------------------------------------------------------------------
Revisar los resultados hechos en Coq
---------------------------------------------------------------------------------------------------------------

Para analizar las demostraciones realizadas en el asistente de pruebas, hay que seguir el procedimiento
para la compilación de bibliotecas:
1. Estar en la raíz de la carpeta src.
2. En el archivo _CoqProject se indican los distintos archivos que están ligados en el proyecto, para compilarlos
hay que crear un archivo Makefile dando la siguiente orden en terminal:

coq_makefile -f _CoqProject -o Makefile

3. Una vez creado el archivo Makefile, dar la orden en terminal:

make

Esto compilará cada uno de los módulos permitiendo su revisión utilizando: La extensión Proof General para el editor Emacs
o el IDE CoqIDE.

---------------------------------------------------------------------------------------------------------------
Modificando el código
---------------------------------------------------------------------------------------------------------------

En caso de modificar el código, habrá que recompilar los módulos esto se logra con el siguiente procedimiento:
1. Limpiar los módulos, esto se logra dando la orden en terminal:

make clean

2. Recompilar, esto se logra dando la orden en terminal:

make

---------------------------------------------------------------------------------------------------------------
Cambio de versión de Coq
---------------------------------------------------------------------------------------------------------------

En caso de actualizar al asistente de pruebas, habrá que seguir el procedimiento:
1. Eliminar los archivos compilados, dando la orden en terminal:

make clean

2. Eliminar el archivo Makefile, dando la orden:

rm Makefile

3. Realizar los pasos de la sección: Revisar los resultados hechos en Coq