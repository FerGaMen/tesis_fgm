(*
* Propiedades sobre las relaciones binarias
* como estructura de álgebra relacional.
*
* Desarrollado por Fernando Abigail Galicia Mendoza
* Contacto: fernandogamen@ciencias.unam.mx
* Coq v8.7
* OCaml v4.05.0
*
* Trabajo desarrollado bajo el marco del proyecto:
* "Tópicos en computación teórica" (PAPIME, PE102117)
* Otorgado por la DGAPA de la UNAM
*
* Referencias:
* 1. Ikeda y Nishimura:
*    "Calculating tree navigation with symmetric relational zipper."
*)
Require Export Lib.Rels.Rel_AR.

(*Distribución de la inversa respecto a la intersección.*)
Remark invProd: ∀ (A:Type) (R S:relation A),
    ((R ∩ S) ~) = ((R ~) ∩ (S ~)).
Proof.
  intros;destruct_eqRel.
  -unfold inv in H.
   inversion H.
   split;assumption.
  -destruct H.
   unfold inv;split;assumption.
Qed.

(*La relación identidad es coreflexiva.*)
Remark corefId: ∀ (A:Type), coref (@id A).
Proof.
  unfold coref;unfold inclusion;intros;assumption.
Qed.

(*La composición izquierda con la relación vacía, resulta en la vacía.*)
Remark compIV: ∀ (A:Type) (R:relation A), (R <∘> vacia) = vacia.
Proof.
  intros;destruct_eqRel.
  -do 2 destruct H;inversion H0.
  -inversion H.
Qed.

(*La composición derecha con la relación vacía, resulta en la vacía.*)
Remark compDV: ∀ (A:Type) (R:relation A), (vacia <∘> R) = vacia.
Proof.
  intros;destruct_eqRel.
  -do 2 destruct H;inversion H.
  -inversion H.
Qed.

(*Primera ley de distribución.*)
Remark distr1: ∀ (A:Type) (R S T:relation A),
    (R <∘> (S ∪ T)) = ((R <∘> S) ∪ (R <∘> T)).
Proof with eauto.
  intros;destruct_eqRel.
  -do 2 destruct H;destruct H0.
   +left;exists x0...
   +right;exists x0...
  -destruct H.
   +do 2 destruct H.
    exists x0...
   +do 2 destruct H.
    exists x0...
Qed.

(*La composición se semidistirbuye bajo la intersección.*)
Remark cSDI: ∀ (A:Type) (R S T:relation A),
    (R <∘> (S ∩ T)) ⊆ ((R <∘> S) ∩ (R <∘> T)).
Proof with eauto.
  unfold inclusion;intros.
  do 2 destruct H.
  destruct H0.
  split.
  -exists x0...
  -exists x0...
Qed.

Hint Resolve cSDI.

(*Ley modular.*)
Lemma lM: ∀ (A:Type) (R S T:relation A),
    ((R <∘> S) ∩ T) ⊆ ((R ∩ (T  <∘> (S ~))) <∘> S).
Proof with eauto.
  intros;unfold inclusion;intros.
  -do 3 destruct H.
   exists x0;split...
   split...
   exists y;split...
Qed.

Hint Resolve lM.

(*El dominio de una relación es correflexiva.*)
Lemma domCoref: ∀ (A:Type) (R:relation A),
    coref (dom R).
Proof with eauto.
  unfold coref;unfold dom;unfold inclusion;intros.
  destruct H...
Qed.

Hint Resolve domCoref.

(*La relaciones coreflexivas son idempotentes.*)
Proposition corefI: ∀ (A:Type) (R:relation A),
    coref R → (R <∘> R) = R.
Proof with eauto.
  intros;destruct_eqRel.
  -do 2 destruct H0;apply H in H0;unfold id in H0.
   rewrite H0...
  -assert (id x y)...
   exists y;split;rewrite H1 in *...
Qed.

Hint Resolve corefI.

(*
 * El producto inverso de una relación coreflexiva y la identidad, 
 * es el producto no inverso.
*)
Remark corefProd: ∀ (A B:Type) (R:relation A),
    coref R → ((@produ B A id R)~) = (id <X> R).
Proof with eauto.
  intros;destruct_eqRel.
  -unfold inv in H0;destruct H0;apply pI.
   +unfold id in *...
   +assert (H2 := H1);apply H in H1;unfold id in H1;rewrite H1 in *...
  -destruct H0;unfold inv;apply pI...
   +unfold id in *...
   +assert (H2 := H1);apply H in H1;unfold id in H1;rewrite H1 in *...
Qed.

Hint Resolve corefProd.
  
(*La composición conmuta para relaciones coreflexivas.*)
Lemma cRCC: ∀ (A:Type) (R S:relation A),
    coref R → coref S → (R <∘> S) = (S <∘> R).
Proof with eauto.
  intros;destruct_eqRel.
  -do 2 destruct H1.
   assert (id x x0 ∧ id x0 y).
   +split...
   +destruct H3;exists y;split;unfold id in H3,H4.
    *rewrite <- H3 in H2...
    *rewrite H4 in H1;rewrite H4 in H3;rewrite H3 in H1...
  -do 2 destruct H1.
   assert (id x x0 ∧ id x0 y).
   +split...
   +destruct H3;exists y;split;unfold id in H3,H4.
    *rewrite <- H3 in H2...
    *rewrite H4 in H1;rewrite H4 in H3;rewrite H3 in H1...
Qed.

Hint Resolve cRCC.

(*
* El operador (\ S) es el izquierdo adjunto del operador
* (S ∪).
*)
Theorem GC_DIFF: ∀ (A:Type) (R S T:relation A),
    ((R \ S) ⊆ T) ↔ (R ⊆ (S ∪ T)).
Proof with eauto.
  intros;unfold iff;split;intros;unfold inclusion in *;intros.
  -apply NNPP;unfold not;intros.
   assert ((compl (S ∪ T)) x y)...
   rewrite dInvC in H2;destruct H2...
  -destruct H0;apply H in H0;destruct H0...
   contradiction.
Qed.

Hint Resolve GC_DIFF.

(*
* El operador de dominio es el adjunto derecho
* de la unión con la relación total.
*)
Theorem GC_DOM: ∀ (A:Type) (R S:relation A),
    coref S → (dom R ⊆ S) ↔ (R ⊆ (total <∘> S)).
Proof with eauto.
  split;intros;unfold inclusion in *;intros.
  -exists y;split...
   apply H0;unfold dom;split.
    *unfold id...
    *exists x...
  -destruct H1;do 2 destruct H2;unfold inv in H2.
   apply H0 in H2;apply H0 in H3;do 2 destruct H2,H3.
   clear H2;clear H3;assert (x1 = x).
   apply H in H4;unfold id in H4...
   rewrite <- H2;rewrite <- H1...
Qed.

Hint Resolve GC_DOM.

(*Hechos sobre la cerradura reflexiva transitiva.*)

(*La cerradura reflexiva-transitiva es reflexiva.*)
Lemma crt_refl: ∀ (A:Type) (R:relation A),
    RRefl (R <*>).
Proof.
  intros;unfold RRefl;intros.
  constructor 1.
  reflexivity.
Qed.

(*La cerradura reflexiva-transitiva es transitiva.*)
Lemma crt_trans: ∀ (A:Type) (R:relation A),
    RTrans (R <*>).
Proof.
  intros;unfold RTrans;intros.
  induction H.
  -rewrite H in *;assumption.
  -constructor 2 with y.
   +assumption.
   +apply IHcerr_refl_trans.
    assumption.
Qed.

(*La quinta definición es transitiva.*)
Lemma crt2_trans: ∀ (A:Type) (R:relation A),
    RTrans (cerr_refl_trans2 R).
Proof.
  intros;unfold RTrans;intros.
  induction H0.
  -rewrite H0 in H;assumption.
  -constructor 2 with y.
   +apply IHcerr_refl_trans2.
    assumption.
   +assumption.
Qed.

(*Las definiciones son equivalentes.*)
Theorem crt_eq_crt2: ∀ (A:Type) (R:relation A),
    (R <*>) = (cerr_refl_trans2 R).
Proof.
  intros;destruct_eqRel.
  -induction H.
   +constructor 1;assumption.
   +apply crt2_trans with y.
    *constructor 2 with x.
     --constructor 1;reflexivity.
     --assumption.
    *assumption.
  -induction H.
   +constructor 1;assumption.
   +apply crt_trans with y.
    *assumption.
    *constructor 2 with z.
     --assumption.
     --constructor 1;reflexivity.
Qed.

(*Menor punto fijo de crt para composición derecha.*)
Lemma crt_crtCompDer: ∀ (A:Type) (R S:relation A),
    ((R <*>) <∘> S) = (cd R S).
Proof.
  intros;destruct_eqRel.
  -do 2 destruct H.
   induction H.
   +rewrite H;constructor 1;assumption.
   +constructor 2 with y0.
    *assumption.
    *apply IHcerr_refl_trans.
     assumption.
  -induction H.
   +exists x;split.
    *constructor 1;reflexivity.
    *assumption.
   +destruct IHcd;destruct H1.
    exists x0;split.
    *constructor 2 with y.
     --assumption.
     --assumption.
    *assumption.
Qed.

(*Menor punto fijo de crt para composición izquierda.*)
Corollary crt_crtCompIzq: ∀ (A:Type) (R S:relation A),
    (S <∘> (R <*>)) = (ci S R).
Proof.
  intros;rewrite crt_eq_crt2;destruct_eqRel.
  -do 2 destruct H.
   induction H0.
   +rewrite <- H0;constructor 1;assumption.
   +constructor 2 with y.
    *apply IHcerr_refl_trans2.
     assumption.
    *assumption.
  -induction H.
   +exists y;split.
    *assumption.
    *constructor 1;reflexivity.
   +destruct IHci;destruct H1.
    exists x0;split.
    *assumption.
    *constructor 2 with y.
     --assumption.
     --assumption.
Qed.

(*Cuando la crt conmuta.*)
Remark crtConm: ∀ (A:Type) (R:relation A),
    ((R <*>) <∘> R) = (R <∘> (R <*>)).
Proof.
  intros;destruct_eqRel.
  -rewrite crt_eq_crt2.
   do 2 destruct H.
   destruct H.
   +rewrite H.
    exists y;split.
    *assumption.
    *constructor 1;reflexivity.
   +exists y0;split.
    *assumption.
    *constructor 2 with z.
     --rewrite <- crt_eq_crt2;assumption.
     --assumption.
  -do 2 destruct H.
   rewrite crt_eq_crt2 in H0.
   induction H0.
    +rewrite <- H0;exists x;split.
     *constructor 1;reflexivity.
     *assumption.
    +exists y;split.
     *constructor 2 with x0.
      --assumption.
      --rewrite crt_eq_crt2;assumption.
     *assumption.
Qed.

(*
 * Si dos relaciones son correflexivas, entonces su unión
 * también es correflexivas.
*)
Lemma COREFL_JOINMEET_1: ∀ (A:Type) (R S:relation A),
    coref R → coref S → coref (R ∪ S).
Proof with eauto.
  intros;unfold coref;unfold inclusion;intros.
  destruct H1...
Qed.

Hint Resolve COREFL_JOINMEET_1.

(*
 * Si dos relaciones son correflexivas, entonces su intersección
 * también es correflexivas.
*)
Lemma COREFL_JOINMEET_2: ∀ (A:Type) (R S:relation A),
    coref R → coref S → coref (R ∩ S).
Proof with eauto.
  intros;unfold coref;unfold inclusion;intros.
  destruct H1...
Qed.

Hint Resolve COREFL_JOINMEET_2.

(*
 * Si dos relaciones son correflexivas, entonces su intersección
 * es igual a su composición.
*)
Lemma COREFL_COMP: ∀ (A:Type) (R S:relation A),
    coref R → coref S → (R ∩ S) = (R <∘> S).
Proof with eauto.
  intros;destruct_eqRel.
  -destruct H1.
   exists x;split...
   apply H0 in H2;unfold id in H2;rewrite <- H2 in H1...
  -destruct H1;destruct H1;split.
   +apply H0 in H2.
    rewrite H2 in H1...
   +apply H in H1.
    rewrite <- H1 in H2...
Qed.

Hint Resolve COREFL_COMP.

(*Conmutación del teorema anterior.*)
Lemma COREFL_COMP_1: ∀ (A:Type) (R S:relation A),
    coref R → coref S → (R ∩ S) = (S <∘> R).
Proof with eauto.
  intros;rewrite cRCC...
Qed.

Hint Resolve COREFL_COMP_1.

(*
 * Una relación es igual a la composición de ella misma con
 * su dominio.
*)
Theorem DOM_DOMAIN: ∀ (A:Type) (R:relation A),
    R = (R <∘> dom R).
Proof.
  intros;destruct_eqRel.
  -exists y;split.
   +assumption.
   +unfold dom.
    split.
    *reflexivity.
    *exists x;split.
     --unfold inv;assumption.
     --assumption.
  -destruct H;destruct H.
   destruct H0.
   rewrite <- H0.
   assumption.
Qed.

Hint Resolve DOM_DOMAIN.

(*Si una relación es correflexiva, su dominio es ella misma.*)
Corollary COREFL_DOM: ∀ (A:Type) (R:relation A),
    coref R → dom R = R.
Proof with eauto.
  intros;destruct_eqRel.
  -destruct H0;destruct H1;destruct H1.
   unfold inv in H1;apply H in H1;unfold id in H1.
   rewrite <- H1...
  -rewrite DOM_DOMAIN with A R in H0.
   do 2 destruct H0;apply H in H0;rewrite <- H0 in H1.
   assumption.
Qed.

Hint Resolve COREFL_DOM.

(*
 * Dada una relación, la composición de esta con la total
 * es igual a la composición de su dominio con la total.
*)
Corollary DOM_RCOND: ∀ (A:Type) (R:relation A),
    (total <∘> dom R) = (total <∘> R).
Proof.
  intros;destruct_eqRel.
  -destruct H;destruct H.
   destruct H0.
   destruct H1;destruct H1.
   rewrite <- H0.
   exists x1;split.
   +constructor.
   +unfold inv in H1;assumption.
  -rewrite DOM_DOMAIN with A R in H.
   rewrite opA in H.
   do 2 destruct H.
   exists x0;split.
   +constructor.
   +assumption.
Qed.

Hint Resolve DOM_RCOND.

(*
 * Dadas R y S, tal que S es inyectiva. El dominio
 * de su composición es lo mismo que la composición
 * de la inversa de S, el dominio de R y S.
*)
Theorem DOM_COMP: ∀ (A:Type) (R S:relation A),
    iny S → dom (R <∘> S) = (S~ <∘> dom R <∘> S).
Proof.
  intros;destruct_eqRel.
  -destruct H0.
   rewrite H0.
   destruct H1;destruct H1.
   unfold inv in H1;destruct H1;destruct H1.
   destruct H2;destruct H2.
   exists x2;split.
   +exists x2;split.
    *unfold inv;assumption.
    *split.
     --reflexivity.
     --exists x0;split.
       ++unfold inv;assumption.
       ++assumption.
   +assumption.
  -destruct H0;destruct H0.
   destruct H0;destruct H0.
   destruct H2.
   destruct H3;destruct H3.
   rewrite H2 in H0.
   rewrite H2 in H3.
   clear H2 x0.
   split.
   +apply H.
    exists y0;split.
    *assumption.
    *assumption.
   +exists x1;split.
    *unfold inv.
     exists y0;split.
     --unfold inv in H3;assumption.
     --unfold inv in H0;assumption.
    *exists y0;split;assumption.
Qed.

Hint Rewrite DOM_COMP.

(*Destrucción de lista de relaciones inyectivas.*)
Remark listInyCons: ∀ (A:Type) (Ss:list (relation A)) (R:relation A) ,
    listIny (R::Ss) → iny R ∧ listIny Ss.
Proof.
  induction Ss.
  -intros;split.
   +apply H;simpl;auto.
   +unfold listIny;intros;contradiction.
  -intros;split.
   +apply H.
    simpl;auto.
   +unfold listIny;intros.
    assert (In S (R :: a :: Ss)).
    *destruct H0.
     --simpl;auto.
     --simpl;auto.
    *apply H in H1.
     assumption.
Qed.

(*
 * Dada una lista de relaciones Ss y una relación
 * R,la crt de Ss está contenida en la crt (R::Ss).
*)
Remark unionesCRT: ∀ (A:Type) (R:relation A)
                     (Ss:list (relation A)),
    (uniones Ss <*>) ⊆ (uniones (R::Ss) <*>).
Proof.
  intros;unfold inclusion;intros.
  induction H.
  -constructor 1.
   assumption.
  -constructor 2 with y.
   +destruct Ss.
    *inversion H.
    *simpl.
     right.
     assumption.
   +assumption.
Qed.

(*
 * Dada R, la crt de su inversa es lo mismo
 * que la inversa de la crt.
*)
Remark invcrt: ∀ (A:Type) (R:relation A),
    ((R ~) <*>) = ((R <*>) ~).
Proof.
  intros;destruct_eqRel.
  -induction H.
   +unfold inv;constructor 1;rewrite H;reflexivity.
   +apply crt_trans with y.
    *trivial.
    *constructor 2 with x.
     --trivial.
     --constructor 1;reflexivity.
  -unfold inv in H.
   induction H.
   +constructor 1;rewrite <- H;reflexivity.
   +apply crt_trans with y.
    *trivial.
    *constructor 2 with x.
     --trivial.
     --constructor 1;reflexivity.
Qed.

(*
 * Destrucción de la uniones de relaciones almacenadas en
 * una lista.
*)
Remark unionDest: ∀ (A:Type) (Si:relation A)
                   (Ss:list (relation A)),
    (uniones (Si::Ss)) = (Si ∪ (uniones Ss)).
Proof.
  intros;destruct_eqRel.
  -destruct Ss.
   +simpl in H.
    left;assumption.
   +destruct H.
    *left;assumption.
    *right;assumption.
  -destruct Ss.
   +destruct H.
    *simpl;assumption.
    *inversion H.
   +destruct H.
    *left;assumption.
    *right;assumption.
Qed.

(*Caso base de la cerradura simétrica.*)
Remark cs_dest: ∀ (A:Type) (R S:relation A),
    (cerr_symmetric R (S::nil)) ⊆ (((S ~) <*>) <∘> R <∘> (S <*>)).
Proof.
  unfold inclusion;intros.
  induction H.
  -exists y;split.
   +exists x;split.
    *constructor 1;reflexivity.
    *assumption.
   +constructor 1;reflexivity.
  -destruct H.
   +rewrite H in *;clear H.
    clear H1.
    destruct IHcerr_symmetric;destruct H.
    do 2 destruct H.
    exists x0;split.
    *exists x1;split.
     --constructor 2 with y;assumption.
     --assumption.
    *apply crt_trans with z.
     --assumption.
     --constructor 2 with w.
       ++assumption.
       ++constructor 1;reflexivity.
   +inversion H.
Qed.

(*
 * Si R es correflexiva y Ss es una lista de 
 * relaciones inyectivas, entonces su cs es coreflexiva.
*)
Lemma cerrSymm_coref: ∀ (A:Type) (R:relation A)
                          (Ss:list (relation A)),
    coref R → listIny Ss →
    coref (cerr_symmetric R Ss).
Proof.
  intros.
  unfold coref;unfold inclusion;intros.
  induction H1.
  -apply H;assumption.
  -rewrite IHcerr_symmetric in H2.
   apply H0 in H1.
   apply H1.
   exists z;split;assumption.
Qed.

(*
 * Dada una relación R, una lista de relaciones Ss
 * y dos elementos x e y. Si R está en Ss y en
 * R se cumple x,y, entonces en las uniones Ss se cumple
 * x,y.
*)
Remark unionIn: ∀ (A:Type) (Si:relation A)
                 (Ss:list (relation A)) (x y:A),
    In Si Ss → Si x y → (uniones Ss) x y.
Proof.
  induction Ss.
  -intros;inversion H.
  -intros.
   destruct H;rewrite unionDest.
   +rewrite H;left;assumption.
   +right;apply IHSs;assumption.
Qed.

(*
 * Dada una lista de relaciones Ss y un par de elementos x,y,
 * si en las uniones de Ss se cumple x,y, entonces existe un Si
 * tal que se cumple x,y en Si.
*)
Remark union_ex: ∀ (A:Type) (Ss:list (relation A)) (x y:A),
    uniones Ss x y ↔ (∃ (Si:relation A), In Si Ss ∧ Si x y).
Proof.
  split.
  induction Ss.
  -intros;inversion H.
  -intros.
   rewrite unionDest in H;destruct H.
   +exists a;split.
    *simpl;left;reflexivity.
    *assumption.
   +apply IHSs in H;do 2 destruct H.
    exists x0;split.
    *simpl;right;assumption.
    *assumption.
  -induction Ss.
   +intros.
    do 2 destruct H.
    inversion H.
   +intros.
    do 2 destruct H.
    destruct H.
    *rewrite H.
     rewrite unionDest;left.
     assumption.
    *rewrite unionDest;right.
     apply IHSs.
     exists x0;split;assumption.
Qed.

(*Destrucción de la función unComp.*)
Remark unComp_Dest: ∀ (A:Type) (Si:relation A)
                     (Ss:list (relation A)),
    unComp (Si::Ss) = (((Si ~) <∘> Si) ∪ (unComp Ss)).
Proof.
  intros;destruct_eqRel.
  destruct Ss.
  -left;assumption.
  -destruct H.
   +left;assumption.
   +right;assumption.
  -destruct Ss;destruct H.
   +assumption.
   +inversion H.
   +left;assumption.
   +right;assumption.
Qed.

(*Destrucción de la función unCompR.*)
Remark unCompR_Dest: ∀ (A:Type) (R Si:relation A)
                      (Ss:list (relation A)),
    unCompR R (Si::Ss) = (((Si ~) <∘> R <∘> Si) ∪ (unCompR R Ss)).
Proof.
  intros;destruct_eqRel.
  destruct Ss.
  -left;assumption.
  -destruct H.
   +left;assumption.
   +right;assumption.
  -destruct Ss;destruct H.
   +assumption.
   +inversion H.
   +left;assumption.
   +right;assumption.
Qed.

(*
 * Dada R una relción, una lista de relaciones Ss y un par de elementos x,y,
 * si en las uniones compuestas de R de Ss se cumple x,y, entonces existe un Si
 * tal que se cumple x,y en Si.
*)
Remark unCompR_ex: ∀ (A:Type) (R:relation A)
                      (Ss:list (relation A)) (x y:A),
    unCompR R Ss x y ↔ (∃ (Si:relation A), In Si Ss /\ ((Si ~) <∘> R <∘> Si) x y).
Proof.
  split.
  induction Ss.
  -intros;inversion H.
  -intros.
   rewrite unCompR_Dest in H;destruct H.
   +exists a;split.
    *simpl;left;reflexivity.
    *assumption.
   +apply IHSs in H;do 2 destruct H.
    exists x0;split.
    *simpl;right;assumption.
    *assumption.
  -induction Ss.
   +intros.
    do 2 destruct H.
    inversion H.
   +intros.
    do 2 destruct H.
    destruct H.
    *rewrite H.
     rewrite unCompR_Dest;left.
     assumption.
    *rewrite unCompR_Dest;right.
     apply IHSs.
     exists x0;split;assumption.
Qed.

(*
 * Dada una relación R, una lista de relaciones Ss
 * y dos elementos x e y. Si R está en Ss y en
 * R se cumple x,y, entonces en las uniones compuestas de R y Ss se cumple
 * x,y.
*)
Remark unIn: ∀ (A:Type) (Si:relation A)
               (Ss:list (relation A)) (x y:A),
    In Si Ss → Si x y → (uniones Ss) x y.
Proof.
  intros.
  induction Ss.
  -inversion H.
  -destruct H;rewrite unionDest.
   +rewrite H;left;assumption.
   +right;apply IHSs;assumption.
Qed.

(*
 * Para una relación R correflexiva, Ss
 * una lista de relaciones inyectivas, se tiene
 * que la composición de la relación total con
 * la cerradura simétrica de R y Ss es igual
 * a la composición de la relación total, R y
 * la cerradura reflexiva-transitiva de las unión
 * de todas las relaciones almacenadas en Ss.
 * Prueba utilizando en su mayoría el mecanismo
 * de Coq.*)
Lemma SYMCLS_RCOND: ∀ (A:Type) (R:relation A)
                            (Ss:list (relation A)),
    coref R → listIny Ss →
    let U := (uniones Ss) in
    (total <∘> (cerr_symmetric R Ss)) =
    (total <∘> R <∘> (U <*>)).
Proof.
  intros;destruct_eqRel.
  -do 2 destruct H1.
   assert ((((U ~) <*>) <∘> R <∘> (U <*>)) x0 y).
   +clear H1.
    induction H2.
    *exists y;split.
     --exists x0;split.
       ++constructor 1;reflexivity.
       ++assumption.
     --constructor 1;reflexivity.
    *clear H3.
     destruct IHcerr_symmetric;destruct H3.
     do 2 destruct H3.
     exists x1;split.
     --exists x2;split.
       ++constructor 2 with y.
         **apply unIn with Si;assumption.
         **assumption.
       ++assumption.
     --rewrite crt_eq_crt2.
       constructor 2 with z.
       ++rewrite <- crt_eq_crt2.
         assumption.
       ++apply unIn with Si;assumption.
   +clear H2.
    rewrite <- opA.
    destruct H3;destruct H2.
    do 2 destruct H2.
    exists x2;split.
    *constructor.
    *exists x1;split;assumption.
  -rewrite crt_crtCompIzq in H1.
   induction H1.
   +do 2 destruct H1.
    exists x0;split.
    *assumption.
    *constructor 1;assumption.
   +clear H1.
    apply union_ex in H2.
    destruct H2;destruct H1.
    assert ((total <∘> (cerr_symmetric R Ss <∘> x0)) x z).
    *rewrite opA; exists y;split;assumption.
    *clear H2 IHci.
     rewrite <- DOM_RCOND in H3.
     rewrite DOM_COMP in H3.
     --rewrite COREFL_DOM in H3.
       ++destruct H3;destruct H2.
         do 4 destruct H3.
         exists x1;split.
         **assumption.
         **constructor 2 with x3 x2 x0;assumption.
       ++apply cerrSymm_coref;assumption.
     --apply H0;assumption.
Qed.

(*
 * Dada una lista de relaciones Ss, su unión U está contenida
 * en la composición de la relación total y el dominio de U.
*)
Corollary gc_dom_gen: ∀ (A:Type) (R:relation A),
    R ⊆ (total <∘> (dom R)).
Proof.
  intros.
  apply GC_DOM.
  -apply domCoref.
  -unfold inclusion;intros;assumption.
Qed.

(*
 * Dadas R una relación cualquiera y Ss una lista
 * de relaciones inyectivas, el dominio de su
 * composición es igual a la cerradura simétrica
 * del dominio de R y la lista Ss.*)
Theorem SYMCLS_DOM: ∀ (A:Type) (R:relation A)
                      (Ss:list (relation A)),
    listIny Ss →
    let U := (uniones Ss) in
    dom (R <∘> (U <*>)) = (cerr_symmetric (dom R) Ss).
Proof.
  intros;apply eqRelEq;unfold same_relation;split.
  -apply GC_DOM.
   +apply cerrSymm_coref.
    *apply domCoref.
    *assumption.
   +rewrite SYMCLS_RCOND.
    *unfold inclusion;intros.
     do 2 destruct H0.
     exists x0;split.
     --apply gc_dom_gen;assumption.
     --unfold U in H1;assumption.
    *apply domCoref.
    *assumption.
  -unfold inclusion;intros.
   induction H0.
   +destruct H0.
    split.
    *assumption.
    *do 2 destruct H1.
     exists x0;split.
     --rewrite invComp.
       exists x;split.
       ++unfold inv;constructor 1;reflexivity.
       ++assumption.
     --exists y;split.
       ++assumption.
       ++constructor 1;reflexivity.
   +clear H2.
    destruct IHcerr_symmetric.
    split.
    *apply H in H0.
     apply H0.
     exists x0;split.
     --assumption.
     --rewrite H2;assumption.
    *do 2 destruct H4.
     exists x1;split.
     --clear H2 H3 H5.
       rewrite invComp in H4.
       destruct H4;destruct H2.
       rewrite invComp.
       unfold inv in H1,H2,H3.
       exists x2;split.
       ++unfold inv.
         rewrite crt_eq_crt2.
         constructor 2 with x0.
         **rewrite <- crt_eq_crt2.
           assumption.
         **apply unIn with Si;assumption.
       ++assumption.
     --clear H2 H1 H4.
       destruct H5;destruct H1.
       exists x2;split.
       ++assumption.
       ++rewrite crt_eq_crt2.
         constructor 2 with y.
         **rewrite <- crt_eq_crt2.
           assumption.
         **apply unIn with Si;assumption.
Qed.