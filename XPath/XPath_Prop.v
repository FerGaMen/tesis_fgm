(*
* Propiedades de simetría.
*
* Desarrollado por Fernando Abigail Galicia Mendoza
* Contacto: fernandogamen@ciencias.unam.mx
* Coq v8.7
* OCaml v4.05.0
*
* Trabajo desarrollado bajo el marco del proyecto:
* "Tópicos en computación teórica" (PAPIME, PE102117)
* Otorgado por la DGAPA de la UNAM
*
* Referencias:
* 1. Ikeda y Nishimura:
*    "Calculating tree navigation with symmetric relational zipper."
*)

Require Export Lib.XPath.XPath_Def.
Import ListNotations.
Open Scope list_scope.

(*
 **********************************************************
 *Sección 4.1
 **********************************************************
*)

Section Sec4_1.

  Variable A:Type.
  Variables a b:A.

  Definition Q1 := (@cerr_symmetric (zipper A)
                      (dn_L <∘> (betaCoref a) <∘> up_L)
                      [up_R]).

  Remark listIny_upR: listIny [@up_R A].
  Proof.
    unfold listIny;intros;destruct H.
    -rewrite <- H;apply upr_iny.
    -inversion H.
  Qed.

  (*
   * Una interpretación de la expresión:
   * child :: β[parent :: α] 
   *)
  Lemma inter_eq:
      ie (child ;; b [[ pathP (parent ;; a) ]] `) =
      ((cerr_symmetric (dn_L <∘> (betaCoref a) <∘> up_L) (up_R::nil))
         <∘>
         (betaCoref b) <∘> (dn_R <*>) <∘> dn_L).
  Proof.
    simpl;rewrite opA with (betaCoref a) up_L (up_R <*>).
    assert (let U1 := uniones [@up_R A] in up_R = U1).
    -simpl;reflexivity.
    -rewrite H.
     rewrite SYMCLS_DOM.
     +rewrite DOM_COMP.
      *rewrite COREFL_DOM.
       --rewrite UPDN_INVERSE_3.
         rewrite <- H;clear H.
         do 2 rewrite opA;reflexivity.
       --apply betaCoref_coref.
      *apply upl_iny.
     +apply listIny_upR.
  Qed.

  (*Lema i para ecuación 4.1*)
  Lemma ec4_1_i:
      ((@up_R A <*>) <∘> (dn_R <*>))
        ⊆
        ((up_R <*>) ∪ (dn_R <*>)).
  Proof.
    unfold inclusion;intros.
    rewrite crt_crtCompDer in H.
    induction H.
    -right;assumption.
    -clear H0;destruct IHcd.
     +left;constructor 2 with x0;assumption.
     +destruct H0.
      *rewrite <- H0;left;constructor 2 with x0.
       --assumption.
       --constructor 1;reflexivity.
      *right;assert (id x y).
       --clear H1;apply dnrinv_dnr_coref.
         rewrite UPDN_INVERSE_2;exists x0;split;assumption.
       --rewrite H2;assumption.
  Qed.

  Lemma sec4_1_QCoref:
    coref Q1.
  Proof.
    apply cerrSymm_coref.
    -unfold coref;unfold inclusion;intros.
     do 4 destruct H.
     apply betaCoref_coref in H1.
     rewrite H1 in H;clear H1.
     apply DnUp_COREFL_3.
     rewrite UPDN_INVERSE_1;exists x0;split;assumption.
    -apply listIny_upR.
  Qed.

  (*Lema ii para la ecuación 4.1*)
  Lemma ec4_1_ii: (dn_R <∘> Q1) ⊆ (Q1 <∘> dn_R).
  Proof.
    unfold inclusion;intros.
    assert ((dn_R <∘> Q1 <∘> up_R <∘> dn_R) x y).
    -do 2 rewrite <- opA;
       rewrite cRCC with (zipper A) Q1 (up_R <∘> dn_R).
     +do 2 rewrite opA.
      rewrite <- UPDN_INVERSE_2.
      rewrite <- UpDn_Triple_2;assumption.
     +apply sec4_1_QCoref.
     +rewrite <- UPDN_INVERSE_2;apply dnrinv_dnr_coref.
    -clear H.
     destruct H0;destruct H.
     exists x0;split.
     +clear H0;do 4 destruct H.
      constructor 2 with x2 x1 up_R.
      *simpl;left;reflexivity.
      *rewrite UPDN_INVERSE_4;assumption.
      *assumption.
      *assumption.
     +assumption.
  Qed.

  (*Ecuación 4.1: La simetría hijo/padre*)
  Theorem ec4_1:
      ie (child ;; b [[ pathP (parent ;; a) ]] `) =
      ie ((self ;; a) // (child ;; b) `).
  Proof.
    rewrite inter_eq;simpl.
    fold Q1;rewrite cRCC with (zipper A) Q1 (betaCoref b).
    -destruct_eqRel.
     +do 2 rewrite <- opA in H.
      do 2 destruct H.
      do 2 rewrite <- opA.
      exists x0;split.
      *assumption.
      *clear H.
       rewrite opA with Q1 (dn_R <*>) dn_L in H0.
       destruct H0;destruct H.
       do 2 destruct H.
       apply cs_dest in H.
       do 4 destruct H.
       assert (((up_R <*>) <∘> (dn_R <*>)) x3 x1).
       --exists x2;split;assumption.
       --clear H2 H1.
         apply ec4_1_i in H4.
         destruct H3;destruct H1.
         assert (id x5 y).
         ++clear H H1.
           destruct H4.
           **rewrite crt_eq_crt2 in H.
             destruct H.
             ---rewrite <- H in H0.
                apply dnlinv_dnl_coref.
                rewrite UPDN_INVERSE_1;exists x1;split;assumption.
             ---assert (vacia y0 y).
                +++rewrite <- UpDn_Emp_2.
                   exists z;split;assumption.
                +++inversion H3.
           **destruct H.
             ---rewrite H in H2.
                apply dnlinv_dnl_coref.
                rewrite UPDN_INVERSE_1;exists y0;split;assumption.
             ---assert (vacia x5 y0).
                +++rewrite <- UpDn_Emp_1.
                   exists x1;split;assumption.
                +++inversion H3.         
         ++clear H2 H0 H4.
           rewrite <- H3.
           clear H3.
           rewrite UPDN_INVERSE_4 in H.
           rewrite opND with (betaCoref a).
           exists x4;split;assumption.
     +do 2 rewrite <- opA in H.
      do 2 destruct H.
      do 2 rewrite <- opA.
      exists x0;split.
      *assumption.
      *clear H.
       rewrite opA in H0.
       rewrite opND with (betaCoref a) in H0.
       rewrite <- opA in H0.
       rewrite crt_crtCompDer in H0.
       induction H0.
       --rewrite UpDn_Triple_1 in H.
         rewrite <- opA with dn_L (dn_L ~) dn_L in H.
         rewrite <- opA with dn_L ((dn_L ~) <∘> dn_L)
                             (betaCoref a) in H.
         rewrite cRCC with (zipper A)
                           ((dn_L ~) <∘> dn_L)
                           (betaCoref a) in H.
         ++do 2 destruct H.
           do 2 destruct H0.
           do 2 destruct H1.
           rewrite UPDN_INVERSE_1 in H1.
           exists x3;split.
           **constructor 1.
             exists x2;split.
             ---exists x1;split;assumption.
             ---assumption.
           **exists x3;split.
             ---constructor 1;reflexivity.
             ---assumption.
         ++apply dnlinv_dnl_coref.
         ++apply betaCoref_coref.
       --clear H0.
         destruct IHcd;destruct H0.
         assert ((dn_R <∘> Q1) x0 x1).
         ++exists y;split;assumption.
         ++clear H H0.
           apply ec4_1_ii in H2.
           destruct H1;destruct H.
           destruct H2;destruct H1.
           exists x3;split.
           **assumption.
           **exists x2;split.
             ---constructor 2 with x1;assumption.
             ---assumption.
    -apply sec4_1_QCoref.
    -apply betaCoref_coref.
  Qed.

End Sec4_1.

(*
**********************************************************
*Sección 4.2
**********************************************************
*)

Section Sec4_2.

  Variable A:Type.
  Variables a b:A.

  Definition Q := (@cerr_symmetric (zipper A)
                     (dn_L <∘> (betaCoref a) <∘> up_L)
                     [up_L;up_R]).
  Definition C := ((@dn_R A <*>) <∘> dn_L).

  Remark listIny_upL_upR:
      listIny [@up_L A;up_R].
  Proof.
    unfold listIny;intros;destruct H.
    -rewrite <- H;apply upl_iny.
    -destruct H.
     +rewrite <- H;apply upr_iny.
     +inversion H.
  Qed.

  Lemma sec4_2_QCoref:
      coref Q.
  Proof.
    apply cerrSymm_coref.
    -unfold coref;unfold inclusion;intros.
     do 4 destruct H.
     destruct H1.
     rewrite H1 in H0.
     apply DnUp_COREFL_3.
     rewrite UPDN_INVERSE_1;exists (Node l i r,p);split;assumption.
    -apply listIny_upL_upR.
  Qed.  
  
  Lemma ec4_3_i_1:
      (dn_L <∘> Q)
        ⊆
        ((dn_L <∘> (betaCoref a) <∘> up_L <∘> dn_L)
           ∪
           ((dn_R <∘> Q <∘> up_R <∘> dn_L)
              ∪
              (dn_L <∘> Q <∘> up_L <∘> dn_L))).
  Proof.
    unfold inclusion;intros.
    rewrite <- opA with (dn_R <∘> Q) up_R dn_L.
    rewrite UpDn_Emp_2.
    rewrite compIV.
    rewrite dC with vacia (dn_L <∘> Q <∘> up_L <∘> dn_L).
    rewrite dN with (dn_L <∘> Q <∘> up_L <∘> dn_L).
    rewrite <- opA with (dn_L <∘> Q) up_L dn_L.
    rewrite <- opA with dn_L Q (up_L <∘> dn_L).
    rewrite cRCC with (zipper A) Q (up_L <∘> dn_L).
    -rewrite opA with dn_L (up_L <∘> dn_L) Q.
     rewrite <- UPDN_INVERSE_1.
     rewrite opA with dn_L (dn_L ~) dn_L.
     rewrite <- UpDn_Triple_1.
     right.
     assumption.
    -apply sec4_2_QCoref.
    -rewrite <- UPDN_INVERSE_1;apply dnlinv_dnl_coref.
  Qed.

  Lemma ec4_3_i_2:
      (dn_R <∘> Q)
        ⊆
        ((dn_L <∘> (betaCoref a) <∘> up_L <∘> dn_R)
           ∪
           ((dn_R <∘> Q <∘> up_R <∘> dn_R)
              ∪
              (dn_L <∘> Q <∘> up_L <∘> dn_R))).
  Proof.
    unfold inclusion;intros.
    rewrite <- opA with (dn_L <∘> Q) up_L dn_R.
    rewrite UpDn_Emp_1.
    rewrite compIV.
    rewrite dN with (dn_R <∘> Q <∘> up_R <∘> dn_R).
    rewrite <- opA with (dn_R <∘> Q) up_R dn_R.
    rewrite <- opA with dn_R Q (up_R <∘> dn_R).
    rewrite cRCC with (zipper A) Q (up_R <∘> dn_R).
    -rewrite opA with dn_R (up_R <∘> dn_R) Q.
     rewrite <- UPDN_INVERSE_2.
     rewrite opA with dn_R (dn_R ~) dn_R.
     rewrite <- UpDn_Triple_2.
     right.
     assumption.
    -apply sec4_2_QCoref.
    -rewrite <- UPDN_INVERSE_2;apply dnrinv_dnr_coref.
  Qed.

  Lemma ec4_3_i_3: (dn_L <∘> Q) ⊆ (Q <∘> dn_L).
  Proof.
    unfold inclusion;intros.
    apply ec4_3_i_1 in H;fold Q in H.
    destruct H.
    -do 2 destruct H.
     exists x0;split.
     +constructor 1;assumption.
     +assumption.
    -destruct H.
     +do 2 destruct H.
      exists x0;split.
      *clear H0.
       do 4 destruct H.
       constructor 2 with x2 x1 up_R.
       --simpl;right;left;reflexivity.
       --rewrite UPDN_INVERSE_4;assumption.
       --assumption.
       --assumption.
      *assumption.
     +do 2 destruct H.
      exists x0;split.
      *clear H0.
       do 4 destruct H.
       constructor 2 with x2 x1 up_L.
       --simpl;left;reflexivity.
       --rewrite <- UPDN_INVERSE_1;assumption.
       --assumption.
       --assumption.
      *assumption.
  Qed.

  Lemma ec4_3_i_4: (dn_R <∘> Q) ⊆ (Q <∘> dn_R).
  Proof.
    unfold inclusion;intros.
    apply ec4_3_i_2 in H;fold Q in H.
    destruct H.
    -do 2 destruct H.
     exists x0;split.
     +constructor 1;assumption.
     +assumption.
    -destruct H.
     +do 2 destruct H.
      exists x0;split.
      *clear H0.
       do 4 destruct H.
       constructor 2 with x2 x1 up_R.
       --simpl;right;left;reflexivity.
       --rewrite <- UPDN_INVERSE_2;assumption.
       --assumption.
       --assumption.
      *assumption.
     +do 2 destruct H.
      exists x0;split.
      *clear H0.
       do 4 destruct H.
       constructor 2 with x2 x1 up_L.
       --simpl;left;reflexivity.
       --rewrite <- UPDN_INVERSE_1;assumption.
       --assumption.
       --assumption.
      *assumption.
  Qed.

  (*Lema i para la ecuación 4.4*)
  Lemma ec4_3_i: (C <∘> Q) ⊆ (Q <∘> C).
  Proof.
    unfold inclusion;intros.
    unfold C in H;rewrite <- opA in H.
    rewrite crt_crtCompDer in H.
    induction H.
    -apply ec4_3_i_3 in H.
     do 2 destruct H.
     exists x0;split.
     +assumption.
     +exists x0;split.
      *constructor 1;reflexivity.
      *assumption.
    -clear H0.
     destruct IHcd;destruct H0.
     assert ((dn_R <∘> Q) x x0).
     +exists y;split;assumption.
     +clear H H0.
      apply ec4_3_i_4 in H2.
      destruct H2;destruct H.
      exists x1;split.
      *assumption.
      *do 2 destruct H1.
       exists x2;split.
       --constructor 2 with x0;assumption.
       --assumption.
  Qed.
  
  Lemma ec4_3_ii_1: (Q <∘> (dn_R <*>)) ⊆ ((dn_R <*>) <∘> Q).
  Proof.
    unfold inclusion;intros.
    rewrite crt_crtCompIzq in H.
    induction H.
    -exists x;split.
     +constructor 1;reflexivity.
     +assumption.
    -clear H.
     destruct IHci;destruct H.
     destruct H1.
     +do 4 destruct H1.
      assert (vacia x1 z).
      *rewrite <- UpDn_Emp_1.
       exists y;split;assumption.
      *inversion H4.
     +destruct H1.
      *rewrite <- H1 in H2,H4.
       assert (vacia z0 z).
       --rewrite <- UpDn_Emp_1.
         exists w;split;assumption.
       --inversion H5.
      *destruct H1.
       --rewrite <- H1 in H2,H4.
         assert (id z0 z).
         ++apply DnUp_COREFL_2.
           rewrite UPDN_INVERSE_4;exists w;split;assumption.
         ++rewrite <- H5.
           exists y;split.
           **rewrite crt_eq_crt2.
             constructor 2 with x0.
             ---rewrite <- crt_eq_crt2;assumption.
             ---rewrite UPDN_INVERSE_4 in H2;assumption.
           **assumption.
       --inversion H1.
  Qed.
    
  (*Lema ii para la ecuación 4.5*)
  Lemma ec4_3_ii: (Q <∘> C) ⊆ (C <∘> (Q ∪ (betaCoref a))).
  Proof.
    unfold inclusion;intros.
    unfold C in H.
    rewrite opA in H.
    do 2 destruct H.
    apply ec4_3_ii_1 in H.
    assert (((dn_R <*>) <∘> Q <∘> dn_L) x y).
    -exists x0;split;assumption.
    -clear H H0.
     rewrite <- opA in H1.
     rewrite crt_crtCompDer in H1.
     induction H1.
     +do 2 destruct H.
      destruct H.
      *do 4 destruct H.
       rewrite distr1.
       right.
       exists x2;split.
       --exists x;split.
         ++constructor 1;reflexivity.
         ++assumption.
       --assert (id x1 y).
         ++apply dnlinv_dnl_coref.
           rewrite UPDN_INVERSE_1;exists y0;split;assumption.
         ++rewrite <- H3;assumption.
      *destruct H.
       --rewrite <- H in H1,H3.
         clear H.
         rewrite distr1.
         left.
         exists y0;split.
         ++exists x;split.
           **constructor 1;reflexivity.
           **rewrite <- UPDN_INVERSE_3;assumption.
         ++assert (id z y).
           **apply dnlinv_dnl_coref.
             rewrite UPDN_INVERSE_1;exists w;split;assumption.
           **fold Q in H2.
             rewrite <- H;assumption.
       --destruct H.
         ++rewrite <- H in H1,H3;clear H.
           assert (vacia z y).
           **rewrite <- UpDn_Emp_2.
             exists w;split;assumption.
           **inversion H.
         ++inversion H.
     +clear H1.
      destruct IHcd;destruct H0.
      do 2 destruct H0.
      exists x1;split.
      *exists x2;split.
       --constructor 2 with y;assumption.
       --assumption.
      *assumption.
  Qed.         

  (*Ecuación 4.3*)
  Lemma ec4_3: ((C <+>) <∘> (betaCoref a) <∘> (C <*>))
                 ⊆
                 (Q <∘> (C <+>)).
  Proof.
    unfold inclusion;intros.
    rewrite <- crtConm with (zipper A) C in H.
    do 2 rewrite <- opA in H.
    rewrite crt_crtCompDer in H.
    induction H.
    -rewrite opA in H.
     rewrite crt_crtCompIzq in H.
     induction H.
     +unfold C in H.
      rewrite <- opA in H.
      rewrite crt_crtCompDer in H.
      induction H.
      *rewrite UpDn_Triple_1 in H.
       rewrite <- opA with dn_L (dn_L ~) dn_L in H.
       rewrite <- opA in H.
       rewrite cRCC with (zipper A)
                         ((dn_L ~) <∘> dn_L)
                         (betaCoref a) in H.
       --do 2 destruct H.
         do 2 destruct H0.
         do 2 destruct H1.
         exists x2;split.
         ++constructor 1.
           exists x1;split.
           **exists x0;split;assumption.
           **rewrite <- UPDN_INVERSE_1;assumption.
         ++exists y;split.
           **exists x2;split.
             ---constructor 1;reflexivity.
             ---assumption.
           **constructor 1;reflexivity.
       --apply dnlinv_dnl_coref.
       --apply betaCoref_coref.
      *clear H0;destruct IHcd;destruct H0.
       assert ((dn_R <∘> Q) x x0).
       --exists y;split;assumption.
       --apply ec4_3_i_4 in H2.
         do 2 destruct H2.
         exists x1;split.
         ++assumption.
         ++clear H2 H0 H.
           destruct H1;destruct H.
           exists x2;split.
           **clear H0.
             do 2 destruct H.
             exists x3;split.
             ---constructor 2 with x0;assumption.
             ---assumption.
           **assumption.
     +clear H;destruct IHci;destruct H.
      exists x0;split.
      *assumption.
      *clear H.
       rewrite <- crtConm.
       exists y;split.
       --clear H0.
         destruct H1;destruct H.
         constructor 2 with x1;assumption.
       --assumption.
    -clear H0.
     destruct IHcd;destruct H0.
     rewrite opA.
     exists x0;split.
     +apply ec4_3_i;fold C Q.
      exists y;split;assumption.
     +do 2 destruct H1.
      constructor 2 with x1;assumption.
  Qed.

  (*Ecuación 4.4*)
  Lemma ec4_4: ((C <+>) <∘> Q) ⊆ (Q <∘> (C <+>)).
  Proof.
    unfold inclusion;intros.
    rewrite <- crtConm in H.
    rewrite <- opA in H.
    rewrite crt_crtCompDer in H.
    rewrite opA.
    induction H.
    -exists y;split.
     +apply ec4_3_i;assumption.
     +constructor 1;reflexivity.
    -clear H0.
     destruct IHcd;destruct H0.
     do 2 destruct H0.
     exists x1;split.
     +clear H2 H1.
      apply ec4_3_i;fold C Q.
      exists y;split;assumption.
     +clear H H0.
      constructor 2 with x0;assumption.
  Qed.

  (*Ecuación 4.5*)
  Lemma ec4_5: (Q <∘> (C <+>))
                 ⊆
                 (((C <+>) <∘> (betaCoref a) <∘> (C <*>))
                    ∪
                    ((C <+>) <∘> Q)).
  Proof.
    unfold inclusion;intros.
    rewrite opA in H.
    rewrite crt_crtCompIzq in H.
    induction H.
    -apply ec4_3_ii in H;fold C Q in H.
     do 2 destruct H.
     destruct H0.
     +right;exists x0;split.
      *exists x0;split.
       --assumption.
       --constructor 1;reflexivity.
      *assumption.
     +left;exists y;split.
      *exists x0;split.
       --exists x0;split.
         ++assumption.
         ++constructor 1;reflexivity.
       --assumption.
      *constructor 1;reflexivity.
    -destruct IHci.
     +do 2 destruct H1.
      left.
      exists x0;split.
      *assumption.
      *rewrite crt_eq_crt2;constructor 2 with y.
       --rewrite <- crt_eq_crt2;assumption.
       --assumption.
     +clear H;assert ((((C <+>) <∘> Q) <∘> C) x z).
      *exists y;split;assumption.
      *clear H0 H1.
       rewrite <- crtConm in H.
       do 2 rewrite <- opA in H.
       rewrite crt_crtCompDer in H.
       induction H.
       --do 2 destruct H.
         apply ec4_3_ii in H0;fold C Q in H0.
         do 2 destruct H0.
         destruct H1.
         ++right;exists x1;split.
           **exists x0;split.
             ---assumption.
             ---constructor 2 with x1.
                +++assumption.
                +++constructor 1;reflexivity.
           **assumption.
         ++left;exists y0;split.
           **exists x1;split.
             ---exists x0;split.
                +++assumption.
                +++constructor 2 with x1.
                   ***assumption.
                   ***constructor 1;reflexivity.
             ---assumption.
           **constructor 1;reflexivity.
       --clear H0;destruct IHcd.
         ++do 4 destruct H0.
           left;exists x1;split.
           **exists x2;split.
             ---clear H2 H1.
                do 2 destruct H0.
                exists x0;split.
                +++assumption.
                +++constructor 2 with x3;assumption.
             ---assumption.
           **assumption.
         ++do 2 destruct H0.
           right;exists x1;split.
           **do 2 destruct H0.
             exists x0;split.
             ---assumption.
             ---constructor 2 with x2;assumption.
           **assumption.
  Qed.

  Lemma upl_uprcrt_cm:
      (((@up_L A) <∘> (up_R <*>)) <+>)
      =
      (up_L <∘> ((uniones [up_L;up_R]) <*>)).
  Proof.
    destruct_eqRel.
    -rewrite crt_crtCompIzq in H.
     induction H.
     +do 2 destruct H.
      exists x0;split.
      *assumption.
      *clear H;simpl.
       induction H0.
       --rewrite <- H;constructor 1;reflexivity.
       --constructor 2 with y.
         ++right;assumption.
         ++assumption.
     +simpl in *;clear H.
      destruct IHci;destruct H.
      exists x0;split.
      *assumption.
      *clear H.
       assert ((((up_L ∪ up_R) <*>)
                  <∘>
                  (up_L <∘> (up_R <*>))) x0 z).
       --exists y;split;assumption.
       --clear H0 H1.
         rewrite crt_crtCompDer in H.
         induction H.
         ++do 2 destruct H.
           constructor 2 with x1.
           **left;assumption.
           **clear H;induction H0.
             ---rewrite <- H;constructor 1;reflexivity.
             ---constructor 2 with y0.
                +++right;assumption.
                +++assumption.
         ++clear H0.
           constructor 2 with y0;assumption.
    -simpl in H.
     rewrite crt_crtCompIzq in H.
     induction H.
     +exists y;split.
      *exists y;split.
       --assumption.
       --constructor 1;reflexivity.
      *constructor 1;reflexivity.
     +clear H.
      rewrite <- crtConm in IHci.
      destruct H0.
      *rewrite <- crtConm.
       exists x0;split.
       --destruct IHci;destruct H0.
         rewrite crt_eq_crt2.
         constructor 2 with x1.
         ++rewrite <- crt_eq_crt2;assumption.
         ++assumption.
       --exists y;split.
         ++assumption.
         ++constructor 1;reflexivity.
      *destruct IHci;destruct H0.
       rewrite <- crtConm.
       exists x1;split.
       --assumption.
       --do 2 destruct H1.
         exists x2;split.
         ++assumption.
         ++rewrite crt_eq_crt2.
           constructor 2 with x0.
           **rewrite <- crt_eq_crt2.
             assumption.
           **assumption.
  Qed.

  Lemma dombetaCoref_upl: 
    (dom (betaCoref a <∘> (@up_L A)))
    =
    (dn_L <∘> (betaCoref a) <∘> up_L).
  Proof.
    destruct_eqRel.
    -unfold dom in H.
     destruct H.
     do 2 destruct H0.
     rewrite invComp with (betaCoref a) up_L in H0.
     do 2 destruct H0.
     exists x0;split.
     +exists x1;split.
      *rewrite UPDN_INVERSE_3 in H0;assumption.
      *clear H0 H1.
       unfold inv in H2.
       destruct H2.
       rewrite H0.
       constructor;reflexivity.
     +clear H0 H2.
      destruct H1;destruct H0.
      destruct H0.
      rewrite <- H0.
      assumption.
    -split.
     +do 4 destruct H.
      apply DnUp_COREFL_3.
      exists x1;split.
      *assumption.
      *destruct H1.
       rewrite <- H1.
       rewrite UPDN_INVERSE_1;assumption.
     +do 4 destruct H.
      exists x0;split.
      *rewrite invComp.
       exists x1;split.
       --rewrite UPDN_INVERSE_3;assumption.
       --unfold inv.
         destruct H1.
         rewrite <- H1.
         constructor;reflexivity.
      *exists x0;split.
       --destruct H1.
         constructor;reflexivity.
       --assumption.
  Qed.

  Lemma domdnl: (dom (@dn_L A)) ⊆ (nodeCoref <X> id).
  Proof.
    unfold inclusion;intros.
    destruct H.
    destruct x,y.
    constructor.
    -inversion H.
     destruct t0.
     +do 2 destruct H0.
      do 2 destruct H1.
      inversion H4.
     +constructor.
      reflexivity.
    -inversion H.
     reflexivity.
  Qed.

  Lemma ec4_2:
    ie (desc ;; b [[ pathP (anc ;; a) ]] `) =
    ie ((unionP
           ((desc_or_self ;; a) // (desc ;; b) `)
           ((self ; <*> [[ pathP (anc ;; a) ]]) // (desc ;; b) `)
       )).
  Proof.
    simpl.
    rewrite upl_uprcrt_cm.
    rewrite opA with (betaCoref a)
                     up_L
                     ((uniones [up_L;up_R]) <*>).
    rewrite SYMCLS_DOM.
    -rewrite dombetaCoref_upl.
     rewrite opA.
     fold Q.
     rewrite cRCC with (zipper A)
                       Q
                       (betaCoref b).
     +destruct_eqRel.
      *rewrite <- opA in H.
       do 2 destruct H.
       apply ec4_5 in H0.
       destruct H0.
       --left.
         rewrite <- opA.
         exists x0;split.
         ++assumption.
         ++rewrite <- opA in H0.
           fold C.
           assumption.
       --right.
         rewrite opND with (nodeCoref <X> id).
         rewrite <- opA.
         exists x0;split.
         ++assumption.
         ++clear H.
           assert (H :=  H0).
           apply ec4_4 in H0.
           rewrite opA.
           exists y;split.
           **assumption.
           **clear H.
             apply domdnl.
             split.
             ---reflexivity.
             ---destruct H0;destruct H.
                rewrite <- crtConm in H0.
                do 2 destruct H0.
                do 2 destruct H1.
                exists x3;split;assumption.
      *destruct H.
       --rewrite <- opA in H.
         do 2 destruct H.
         rewrite <- opA.
         exists x0;split.
         ++assumption.
         ++clear H.
           apply ec4_3.
           rewrite <- opA.
           fold C in H0.
           assumption.
       --rewrite <- opA in H.
         do 2 destruct H.
         do 2 rewrite <- opA.
         exists x0;split.
         ++assumption.
         ++clear H.
           rewrite opA in H0.
           destruct H0;destruct H.
           rewrite opND in H0.
           assert (id x1 y).
           **clear H.
             destruct H0.
             destruct H.
             rewrite H.
             rewrite H0.
             reflexivity.
           **rewrite <- H1.
             clear H0 H1.
             fold C in *.
             apply ec4_4 in H.
             rewrite opA with (dn_R <*>) dn_L (C <*>).
             fold C.
             assumption.
     +apply sec4_2_QCoref.
     +apply betaCoref_coref.
    -apply listIny_upL_upR.
  Qed.
         
  Corollary ec4_6:
    ie (\ (desc ;; b [[ pathP (anc ;; a) ]])) =
    ie (\ ((desc_or_self ;; a) // (desc ;; b))).
  Proof.
    intros.
    transitivity ((ie ((desc ;; b [[ pathP (anc ;; a) ]]) `))
                    <∘>
                    (id <X> topCoref)
                    <∘> ((up_L ∪ up_R) <*>)).
    -reflexivity.
    -rewrite ec4_2.
     simpl;fold C Q.
     destruct_eqRel.
     +do 4 destruct H.
      destruct H.
      *exists x0;split.
       --exists y0;split;assumption.
       --assumption.
      *rewrite <- opA in H.
       do 2 destruct H.
       do 2 destruct H2.
       do 2 destruct H3.
       assert (id x3 y0).
       --clear H H2 H3 H1 H0.
         rewrite opND in H4.
         destruct H4.
         destruct H.
         rewrite H.
         rewrite H0.
         reflexivity.
       --clear H4.
         rewrite H5 in H3;clear H5.
         clear H H2 H0.
         rewrite upl_uprcrt_cm in H3.
         rewrite opA with (betaCoref a)
                          up_L
                          ((uniones [up_L;up_R]) <*>)
           in H3.
         rewrite SYMCLS_DOM in H3.
         ++rewrite dombetaCoref_upl in H3;fold Q in H3.
           destruct H3.
           **do 4 destruct H.
             assert (vacia x4 x0).
             ---rewrite <- Up_Emp_1;exists y0;split;assumption.
             ---inversion H3.
           **fold Q in H3;destruct H.
             ---rewrite <- H in H2.
                assert (vacia z x0).
                +++rewrite <- Up_Emp_1;exists w;split;assumption.
                +++inversion H4.
             ---destruct H.
                +++rewrite <- H in H2.
                   assert (vacia z x0).
                   ***rewrite <- Up_Emp_2;exists w;split;assumption.
                   ***inversion H4.
                +++inversion H.
         ++apply listIny_upL_upR.
     +do 4 destruct H.
      exists x0;split.
      *exists x1;split.
       --left;assumption.
       --assumption.
      *assumption.
  Qed.
  
End Sec4_2.