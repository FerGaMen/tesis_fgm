(*
* Propiedades de simetría para el caso de la negación.
*
* Desarrollado por Fernando Abigail Galicia Mendoza
* Contacto: fernandogamen@ciencias.unam.mx
* Coq v8.7
* OCaml v4.05.0
*
* Trabajo desarrollado bajo el marco del proyecto:
* "Tópicos en computación teórica" (PAPIME, PE102117)
* Otorgado por la DGAPA de la UNAM
*
* Referencias:
* 1. Ikeda y Nishimura:
*    "Calculating tree navigation with symmetric relational zipper."
*)

Require Import Lib.XPath.XPath_Def.
Require Import Lib.XPath.XPath_Prop.
Import ListNotations.
Open Scope list_scope.

Section Sec5.
  
  Variable A:Type.
  Variables a b:A.

  Definition C5 := (C A).
  Definition Q5 := (Q A a).
  Definition R1 := (((¬) dom up_L)
                      ∪ (dn_L
                           <∘> ((¬) (betaCoref a)) <∘> up_L)).
  Definition R2 := (((¬) dom up_L)
                      ∪ (dn_L
                           <∘> ((¬) Q5) <∘> up_L)).
  Definition R3 := (((¬) dom up_R)
                      ∪ (dn_R
                           <∘> ((¬) Q5) <∘> up_R)).

  Example ej3:
    ie (desc ;; b [[ negation (pathP (anc ;; a)) ]] `) =
    (((¬) (cerr_symmetric
             (dom (betaCoref a <∘> up_L))
             [up_L;up_R]))
       <∘>
       ((betaCoref b) <∘> (((dn_R <*>) <∘> dn_L) <+>))).
  Proof.
    intros;simpl.
    rewrite upl_uprcrt_cm.
    rewrite  opA with (betaCoref a) up_L
                      ((uniones [up_L;up_R]) <*>).
    rewrite SYMCLS_DOM.
    -reflexivity.
    -apply listIny_upL_upR.
  Qed.

  Lemma dnrS_dnl_m:
    (C5 <+>) = (((uniones [dn_R;dn_L]) <*>)
                      <∘> dn_L).
  Proof.
    intros;simpl;destruct_eqRel.
    -rewrite crt_crtCompIzq in H.
     induction H.
     +do 2 destruct H.
      exists x0;split.
      *clear H0;induction H.
       --rewrite <- H;constructor 1;reflexivity.
       --constructor 2 with y0.
         ++left;assumption.
         ++assumption.       
      *assumption.
     +destruct IHci;destruct H1.
      clear H.
      destruct H0;destruct H.
      exists x1;split.
      *clear H0.
       apply crt_trans with x0.
       --assumption.
       --clear H1.
         assert ((dn_L <∘> (dn_R <*>)) x0 x1).
         exists y;split;assumption.
         clear H H2.
         rewrite crt_crtCompIzq in H0.
         induction H0.
         ++constructor 2 with y0.
           **right;assumption.
           **constructor 1;reflexivity.
         ++rewrite crt_eq_crt2.
           constructor 2 with y0.
           **rewrite <- crt_eq_crt2;assumption.
           **left;assumption.
      *assumption.
    -rewrite crt_crtCompDer in H.
     induction H.
     +exists y;split.
      *exists x;split.
       --constructor 1;reflexivity.
       --assumption.
      *constructor 1;reflexivity.
     +clear H0.
      destruct H.
      *destruct IHcd;destruct H0.
       exists x0;split.
       --do 2 destruct H0.
         exists x1;split.
         ++constructor 2 with y;assumption.
         ++assumption.
       --assumption.
      *destruct IHcd;destruct H0.
       exists y;split.
       --exists x;split.
         ++constructor 1;reflexivity.
         ++assumption.
       --constructor 2 with x0;assumption.
  Qed.

  Lemma dnrS_dnl_NbetaA_m:
      ((C5 <∘> ((¬) (betaCoref a))) <+>)
         = (((dn_R
                ∪ (dn_L
                     <∘> ((¬) (betaCoref a)))) <*>)
              <∘>
              dn_L <∘> ((¬) (betaCoref a))).
  Proof.
    intros;destruct_eqRel.
    -unfold C5,C in H.
     rewrite <- opA with (dn_R <*>)
                         dn_L
                         ((¬) betaCoref a) in H.
     rewrite crt_crtCompIzq in H.
     induction H.
     +do 2 destruct H.
      do 2 destruct H0.
      exists x1;split.
      *exists x0;split.
       --clear H0 H1.
         induction H.
         ++rewrite <- H;constructor 1;reflexivity.
         ++constructor 2 with y0.
           **left;assumption.
           **assumption.
       --assumption.
      *assumption.
     +destruct IHci;destruct H1.
      clear H.
      destruct H1;destruct H.
      do 2 destruct H0.
      do 2 destruct H3.
      exists x3;split.
      *exists x2;split.
       --apply crt_trans with x1.
         ++assumption.
         ++clear H H3 H4.
           assert ((dn_L
                      <∘> ((¬) (betaCoref a))
                      <∘> (dn_R <*>)) x1 x2).
           **exists y;split.
             ---exists x0;split;assumption.
             ---assumption.
           **clear H0 H1 H2.
             rewrite crt_crtCompIzq in H.
             induction H.
             ---constructor 2 with y0.
                +++right;assumption.
                +++constructor 1;reflexivity.
             ---rewrite crt_eq_crt2.
                constructor 2 with y0.
                +++rewrite <- crt_eq_crt2;assumption.
                +++left;assumption.
       --assumption.
      *assumption.
    -rewrite <- opA with ((dn_R
                             ∪
                             (dn_L
                                <∘> ((¬) (betaCoref a)))) <*>)
                         dn_L ((¬) betaCoref a) in H.
     rewrite crt_crtCompDer in H.
     induction H.
     +do 2 destruct H.
      exists y;split.
      *exists x0;split.
       --exists x;split.
         ++constructor 1;reflexivity.
         ++assumption.
       --assumption.
      *constructor 1;reflexivity.
     +clear H0.
      destruct H.
      *destruct IHcd;destruct H0.
       exists x0;split.
       --do 4 destruct H0.
         exists x1;split.
         ++exists x2;split.
           **constructor 2 with y;assumption.
           **assumption.
         ++assumption.
       --assumption.
      *destruct IHcd;destruct H0.
       do 2 destruct H0.
       exists y;split.
       --clear H0 H2 H1.
         do 2 destruct H.
         exists x2;split.
         ++exists x;split.
           **constructor 1;reflexivity.
           **assumption.
         ++assumption.
       --constructor 2 with x0.
         ++exists x1;split;assumption.
         ++assumption.
  Qed.

  Lemma Q_mucomp:
    Q5 = ((dn_L <∘> (betaCoref a) <∘> up_L)
            ∪
            (dn_L <∘> Q5 <∘> up_L)
            ∪
            (dn_R <∘> Q5 <∘> up_R)).
  Proof.
    intros;destruct_eqRel.
    -destruct H.
     +left;left;assumption.
     +destruct H.
      *rewrite <- H in H0,H2;clear H.
       left;right.
       exists z;split.
       --exists y;split.
         ++rewrite UPDN_INVERSE_3 in H0;assumption.
         ++assumption.
       --assumption.
      *destruct H.
       --rewrite <- H in H0,H2;clear H.
         right.
         exists z;split.
         ++exists y;split.
           **rewrite UPDN_INVERSE_4 in H0;assumption.
           **assumption.
         ++assumption.
       --contradiction.
    -destruct H.
     +destruct H.
      *constructor 1;assumption.
      *do 4 destruct H.
       constructor 2 with x1 x0 up_L.
       --simpl;left;reflexivity.
       --rewrite UPDN_INVERSE_3;assumption.
       --assumption.
       --assumption.
     +do 4 destruct H.
      constructor 2 with x1 x0 up_R.
      --simpl;right;left;reflexivity.
      --rewrite UPDN_INVERSE_4;assumption.
      --assumption.
      --assumption.
  Qed.

  Remark R1_coref:
    coref R1.
  Proof.
    unfold coref;unfold inclusion;intros.
    destruct H.
    -apply NEG_COREFL in H;assumption.
    -do 4 destruct H.
     apply NEG_COREFL in H1.
     rewrite H1 in H.
     apply uplinv_upl_coref.
     exists x0;split.
     +rewrite UPDN_INVERSE_3;assumption.
     +assumption.
  Qed.

  Remark R2_coref:
    coref R2.
  Proof.
    unfold coref;unfold inclusion;intros.
    destruct H.
    -apply NEG_COREFL in H;assumption.
    -do 4 destruct H.
     apply NEG_COREFL in H1.
     rewrite H1 in H.
     apply uplinv_upl_coref.
     exists x0;split.
     +rewrite UPDN_INVERSE_3;assumption.
     +assumption.
  Qed.

  Remark R3_coref:
      coref R3.
  Proof.
    unfold coref;unfold inclusion;intros.
    destruct H.
    -apply NEG_COREFL in H;assumption.
    -do 4 destruct H.
     apply NEG_COREFL in H1.
     rewrite H1 in H.
     apply uprinv_upr_coref.
     exists x0;split.
     +rewrite UPDN_INVERSE_4;assumption.
     +assumption.
  Qed.

  Lemma Neg_QDest:
    ((¬) Q5) = (R1 ∩ R2 ∩ R3).
  Proof.
    intros.
    rewrite Q_mucomp.
    rewrite NEG_DEMORGAN_1.
    rewrite NEG_DEMORGAN_1.
    destruct_eqRel.
    -do 2 destruct H;split.
     +split.
      *unfold R1.
       clear H1 H0.
       rewrite <- COREFL_DOM with (zipper A)
                                  (betaCoref a).
       --rewrite <- invI with dn_L.
         rewrite UPDN_INVERSE_1.
         rewrite <- NEG_DOM.
         ++destruct H.
           constructor.
           **assumption.
           **unfold not;intros;apply H0.
             clear H0.
             destruct H1.
             do 2 destruct H1.
             rewrite invComp in H1.
             do 2 destruct H1.
             unfold inv in H3.
             apply betaCoref_coref in H3.
             rewrite <- H3 in H1.
             rewrite UPDN_INVERSE_3 in H1.
             do 2 destruct H2.
             exists x2;split.
             ---exists x0;split;assumption.
             ---assumption.
         ++apply upl_iny.
         ++apply DnUp_COREFL_1.
       --apply betaCoref_coref.
      *unfold R2,Q5,Q.
       clear H H0.
       rewrite <- COREFL_DOM with (zipper A)
                                  (cerr_symmetric
                                     (dn_L
                                        <∘> (betaCoref a)
                                        <∘> up_L)
                                     [up_L;up_R]).
       --rewrite <- UPDN_INVERSE_3.
         rewrite <- NEG_DOM.
         ++destruct H1.
           constructor.
           **assumption.
           **unfold not;intros;apply H0.
             clear H0.
             destruct H1.
             do 2 destruct H1.
             rewrite invComp in H1.
             do 2 destruct H1.
             do 2 destruct H2.
             rewrite UPDN_INVERSE_3 in H3.
             apply sec4_2_QCoref in H3.
             rewrite <- H3 in H1.
             rewrite UPDN_INVERSE_3 in H1.
             exists x2;split.
             ---exists x0;split.
                +++assumption.
                +++rewrite UPDN_INVERSE_3 in H2;assumption.
             ---assumption.
         ++apply upl_iny.
         ++apply DnUp_COREFL_1.
       --apply sec4_2_QCoref.
     +unfold R3,Q5,Q.
      clear H H1.
      rewrite <- COREFL_DOM with (zipper A)
                                 (cerr_symmetric
                                    (dn_L
                                       <∘> (betaCoref a)
                                       <∘> up_L)
                                    [up_L;up_R]).
      --rewrite <- UPDN_INVERSE_4.
        rewrite <- NEG_DOM.
        ++destruct H0.
          constructor.
          **assumption.
          **unfold not;intros;apply H0.
            clear H0.
            destruct H1.
            do 2 destruct H1.
            rewrite invComp in H1.
            do 2 destruct H1.
            do 2 destruct H2.
            rewrite UPDN_INVERSE_4 in H1.
            apply sec4_2_QCoref in H3.
            rewrite <- H3 in H1.
            exists x2;split.
            ---exists x0;split.
               +++assumption.
               +++assumption.
            ---assumption.
        ++apply upr_iny.
        ++apply DnUp_COREFL_2.
      --apply sec4_2_QCoref.
    -do 2 destruct H.
     split.
     +split.
      *unfold R1 in H.
       clear H1 H0.     
       rewrite <- COREFL_DOM with (zipper A)
                                  (betaCoref a) in H.
       --rewrite <- UPDN_INVERSE_3 in H.
         rewrite <- NEG_DOM in H.
         ++destruct H.
           constructor.
           **assumption.
           **unfold not;intros.
             apply H0;clear H0.
             constructor.
             ---assumption.
             ---destruct H1;destruct H0.
                do 2 destruct H0.
                exists x0;split.
                +++rewrite invComp.
                   exists x1;split.
                   ***rewrite UPDN_INVERSE_3;assumption.
                   ***destruct H2.
                      rewrite H2.
                      constructor;reflexivity.
                +++exists x0;split.
                   ***destruct H2.
                      constructor;reflexivity.
                   ***assumption.
         ++apply upl_iny.
         ++apply DnUp_COREFL_1.
       --apply betaCoref_coref.
      *unfold R2,Q5,Q in H1.
       clear H H0.
       rewrite <- COREFL_DOM with (zipper A)
                                  (cerr_symmetric
                                     (dn_L
                                        <∘> (betaCoref a)
                                        <∘> up_L)
                                     [up_L;up_R]) in H1.
       --rewrite <- UPDN_INVERSE_3 in H1.
         rewrite <- NEG_DOM in H1.
         ++destruct H1.
           constructor.
           **assumption.
           **unfold not;intros.
             apply H0;clear H0.
             constructor.
             ---assumption.
             ---destruct H1;destruct H0.
                do 2 destruct H0.
                exists x1;split.
                +++rewrite invComp.
                   exists x1;split.
                   ***rewrite UPDN_INVERSE_3;assumption.
                   ***assert (id x1 x0).
                      ----apply sec4_2_QCoref in H2.
                          assumption.
                      ----rewrite <- H3 in H2.
                          rewrite UPDN_INVERSE_3.
                          assumption.
                +++exists x0;split.
                   ***rewrite UPDN_INVERSE_3.
                      assumption.
                   ***assumption.
         ++apply upl_iny.
         ++apply DnUp_COREFL_1.
       --apply sec4_2_QCoref.
     +unfold R3,Q5,Q in H0.
      clear H H1.
      rewrite <- COREFL_DOM with (zipper A)
                                 (cerr_symmetric
                                    (dn_L
                                       <∘> (betaCoref a)
                                       <∘> up_L)
                                    [up_L;up_R]) in H0.
      --rewrite <- invI with dn_L in H0.
        rewrite <- UPDN_INVERSE_3 in H0.
        rewrite <- UPDN_INVERSE_4 in H0.
        rewrite <- NEG_DOM in H0.
        ++destruct H0.
          constructor.
          **assumption.
          **unfold not;intros.
            apply H0;clear H0.
            constructor.
            ---assumption.
            ---destruct H1;destruct H0.
               do 2 destruct H0.
               exists x1;split.
               +++rewrite invComp.
                  exists x1;split.
                  ***rewrite UPDN_INVERSE_4;assumption.
                  ***assert (id x1 x0).
                     ----apply sec4_2_QCoref in H2.
                         assumption.
                     ----rewrite <- H3 in H2.
                         rewrite UPDN_INVERSE_3.
                         assumption.
               +++exists x0;split.
                  ***rewrite UPDN_INVERSE_3.
                     assumption.
                  ***assumption.
        ++apply upr_iny.
        ++apply DnUp_COREFL_2.
      --apply sec4_2_QCoref.
  Qed.

  Remark negDomUpL_dnL_Vacia:
      (((¬) dom (@up_L A)) <∘> dn_L) = vacia.
  Proof.
    intros;destruct_eqRel.
    -rewrite <- UPDN_INVERSE_3 in H.
     do 2 destruct H.
     rewrite DOM_DOMAIN with (zipper A) up_L in H0.
     rewrite invComp in H0.
     do 2 destruct H0.
     assert ((dom up_L) x0 x1).
     +clear H H1.
      unfold inv in H0.
      destruct H0.
      split.
      *symmetry;assumption.
      *do 2 destruct H0.
       exists x1;split;assumption.
     +clear H0.
      assert ((((¬) dom up_L) <∘> (dom up_L)) x x1).
      *exists x0;split;assumption.
      *clear H H2.
       rewrite <- COREFL_COMP in H0.
       --rewrite cC in H0.
         rewrite NEG_EXMDL_2 with (zipper A) (dom up_L) in H0.
         contradiction.
       --unfold coref;unfold inclusion;intros.
         destruct H;assumption.
       --apply domCoref.
    -contradiction.
  Qed.

  Lemma R1_dnL:
    (R1 <∘> dn_L) = (dn_L <∘> ((¬) betaCoref a)).
  Proof.
    intros.
    transitivity ((((¬) dom up_L) <∘> dn_L)
                    ∪
                    (dn_L <∘> ((¬) betaCoref a))).
    -unfold R1.
     rewrite sumProd.
     rewrite <- opA with ((@dn_L A) <∘> ((¬) betaCoref a)) up_L dn_L.
     destruct_eqRel.
     +destruct H.
      *rewrite negDomUpL_dnL_Vacia in H;contradiction.
      *do 2 destruct H.
       rewrite <- UPDN_INVERSE_1 in H0.
       apply dnlinv_dnl_coref in H0.
       rewrite H0 in H.
       right;assumption.
     +destruct H.
      *rewrite negDomUpL_dnL_Vacia in H;contradiction.
      *right;exists y;split.
       --assumption.
       --do 2 destruct H.
         destruct H0.
         rewrite H0 in H.
         rewrite <- UPDN_INVERSE_1.
         exists x;split;assumption.
    -destruct_eqRel.
     +destruct H.
      *rewrite negDomUpL_dnL_Vacia in H;contradiction.
      *assumption.
     +right;assumption.
  Qed.

  Lemma R2_dnL:
    (R2 <∘> dn_L) = (dn_L <∘> ((¬) Q5)).
  Proof.
    intros.
    transitivity ((((¬) dom up_L) <∘> dn_L)
                    ∪
                    (dn_L <∘> ((¬) Q5))).
    -unfold R2.
     rewrite sumProd.
     rewrite <- opA with ((@dn_L A) <∘> ((¬) Q5)) up_L dn_L.
     destruct_eqRel.
     +destruct H.
      *rewrite negDomUpL_dnL_Vacia in H;contradiction.
      *do 2 destruct H.
       rewrite <- UPDN_INVERSE_1 in H0.
       apply dnlinv_dnl_coref in H0.
       rewrite H0 in H.
       right;assumption.
     +destruct H.
      *rewrite negDomUpL_dnL_Vacia in H;contradiction.
      *right;exists y;split.
       --assumption.
       --do 2 destruct H.
         destruct H0.
         rewrite H0 in H.
         rewrite <- UPDN_INVERSE_1.
         exists x;split;assumption.
    -destruct_eqRel.
     +destruct H.
      *rewrite negDomUpL_dnL_Vacia in H;contradiction.
      *assumption.
     +right;assumption.
  Qed.

  Remark dnL_cont_negDomUpRDnL:
      (@dn_L A) ⊆ (((¬) dom up_R) <∘> dn_L).
  Proof.
    unfold inclusion;intros.
    rewrite <- UPDN_INVERSE_3 in H.
    rewrite DOM_DOMAIN with (zipper A) up_L in H.
    rewrite invComp in H.
    do 2 destruct H.
    exists x0;split.
    -clear H0.
     assert ((dom up_L) x x0).
     +unfold inv in H;destruct H;split.
      *symmetry;assumption.
      *rewrite H;rewrite H in H0;assumption.
     +clear H.
      rewrite <- NEG_DBLNEG with (zipper A) (dom up_L) in H0.
      *revert H0.
       revert x x0;clear y.
       apply GC_DIFF.
       rewrite <- NEG_DEMORGAN_2.
       unfold inclusion;intros.
       split.
       --assumption.
       --unfold not;intros.
         destruct H0.
         destruct H0.
         destruct H1.
         assert ((((up_L ~) <∘> up_L) ∩
                                      ((up_R ~) <∘> up_R)) x y).
         ++split;assumption.
         ++clear H H0 H2 H1 H3.
           rewrite COREFL_COMP in H4.
           **rewrite UPDN_INVERSE_3 in H4.
             rewrite UPDN_INVERSE_4 in H4.
             rewrite <- opA in H4.
             destruct H4;destruct H.
             rewrite opA in H0.
             do 2 destruct H0.
             rewrite UpDn_Emp_1 in H0.
             contradiction.
           **apply uplinv_upl_coref.
           **apply uprinv_upr_coref.
      *apply domCoref.
    -rewrite UPDN_INVERSE_3 in H0.
     assumption.
  Qed.

  Lemma R3_dnL:
    (R3 <∘> dn_L) = dn_L.
  Proof.
    intros.
    unfold R3.
    rewrite sumProd.
    destruct_eqRel.
    -destruct H.
     *do 2 destruct H.
      apply dnL_cont_negDomUpRDnL in H0.
      do 2 destruct H0.
      destruct H,H0.
      rewrite <- H in H0.
      rewrite <- H0 in H1.
      assumption.
     *rewrite <- opA in H.
      do 2 destruct H.
      rewrite UpDn_Emp_2 in H0.
      contradiction.
    -apply dnL_cont_negDomUpRDnL in H.
     left;assumption.
  Qed.

  Lemma R1_dnR:
    (R1 <∘> dn_R) = dn_R.
  Proof.
    intros.
    unfold R1;rewrite sumProd;rewrite <- opA with ((@dn_L A) <∘> ((¬) betaCoref a)) up_L dn_R.
    destruct_eqRel.
    -destruct H.
     +do 3 destruct H;rewrite H;assumption.
     +do 2 destruct H.
      rewrite UpDn_Emp_1 in H0.
      contradiction.
    -left.
     rewrite <- UPDN_INVERSE_4 in H.
     rewrite DOM_DOMAIN with (zipper A) up_R in H.
     rewrite invComp in H.
     do 2 destruct H.
     rewrite UPDN_INVERSE_4 in H0.
     assert ((dom (@up_R A)) x x0).
     +destruct H;split.
      *symmetry;assumption.
      *rewrite H in H1;rewrite H;assumption.
     +clear H;exists x0;split.
      *clear H0.
       rewrite <- NEG_DBLNEG with (zipper A) (dom up_R) in H1.
       --revert H1.
         revert x x0;clear y.
         apply GC_DIFF.
         rewrite <- NEG_DEMORGAN_2.
         unfold inclusion;intros.
         split.
         ++assumption.
         ++unfold not;intros.
           destruct H0.
           destruct H0.
           destruct H1.
           do 2 destruct H2.
           do 2 destruct H3.
           assert (vacia x0 x1).
           **rewrite <- UpDn_Emp_2.
             exists y;split.
             ---assumption.
             ---rewrite <- UPDN_INVERSE_3;assumption.
           **contradiction.
       --apply domCoref.
      *assumption.
  Qed.

  Lemma R2_dnR:
    (R2 <∘> dn_R) = dn_R.
  Proof.
    intros.
    unfold R2;rewrite sumProd;rewrite <- opA.
    destruct_eqRel.
    -destruct H.
     +do 3 destruct H;rewrite H;assumption.
     +do 2 destruct H.
      rewrite UpDn_Emp_1 in H0.
      contradiction.
    -left.
     rewrite <- UPDN_INVERSE_4 in H.
     rewrite DOM_DOMAIN with (zipper A) up_R in H.
     rewrite invComp in H.
     do 2 destruct H.
     rewrite UPDN_INVERSE_4 in H0.
     exists x0;split.
     +clear H0.
      assert ((dom (@up_R A)) x x0).
      *destruct H;split.
       --symmetry;assumption.
       --rewrite H in H0;rewrite H;assumption.
      *clear H.
       rewrite <- NEG_DBLNEG with (zipper A) (dom up_R) in H0.
       --revert H0.
         revert x x0;clear y.
         apply GC_DIFF.
         rewrite <- NEG_DEMORGAN_2.
         unfold inclusion;intros.
         split.
         ++assumption.
         ++unfold not;intros.
           destruct H0.
           destruct H0.
           destruct H1.
           do 2 destruct H2.
           do 2 destruct H3.
           assert (vacia x0 x1).
           **rewrite <- UpDn_Emp_2.
             exists y;split.
             ---assumption.
             ---rewrite <- UPDN_INVERSE_3;assumption.
           **contradiction.
       --apply domCoref.
     +assumption.
  Qed.

  Lemma R3_dnR:
    (R3 <∘> dn_R) = (dn_R <∘> ((¬) Q5)).
  Proof.
    intros.
    unfold R3;rewrite sumProd.
    destruct_eqRel.
    -destruct H.
     +do 2 destruct H.
      rewrite <- UPDN_INVERSE_4 in H0.
      rewrite DOM_DOMAIN with (zipper A) up_R in H0.
      rewrite invComp in H0.
      do 2 destruct H0.
      assert ((dom up_R) x0 x1).
      *clear H H1.
       destruct H0;split.
       --symmetry;assumption.
       --rewrite H in *;assumption.
      *clear H0.
       assert ((((¬) dom up_R) ∩ (dom up_R)) x x1).
       --clear H1.
         rewrite COREFL_COMP.
         ++exists x0;split;assumption.
         ++apply NEG_COREFL.
         ++apply domCoref.
       --clear H H1.
         rewrite cC in H0;rewrite NEG_EXMDL_2 in H0;contradiction.
     +rewrite <- opA in H;do 2 destruct H.
      rewrite <- UPDN_INVERSE_4 in H0.
      apply DnUp_COREFL_2 in H0.
      rewrite H0 in H;assumption.
    -right.
     rewrite <- opA.
     exists y;split.
     +assumption.
     +do 2 destruct H.
      destruct H0.
      rewrite H0 in H.
      exists x;split.
      *rewrite <- UPDN_INVERSE_2.
       assumption.
      *assumption.
  Qed.

  Lemma lema_i:
    (((¬) Q5) <∘> dn_R) = (dn_R <∘> ((¬) Q5)).
  Proof.
    rewrite <- R3_dnR.
    rewrite <- R2_dnR.
    rewrite <- R1_dnR.
    transitivity (((¬) Q5) <∘> dn_R).
    -rewrite R1_dnR.
     rewrite R2_dnR;reflexivity.
    -rewrite Neg_QDest;destruct_eqRel.
     +do 4 destruct H.
      exists y0;split.
      *assumption.
      *exists x;split.
       --apply R1_coref in H.
         rewrite H in H2.
         rewrite H.
         assumption.
       --exists y0;split;assumption.
     +do 2 destruct H.
      do 2 destruct H0.
      do 2 destruct H1.
      exists x2;split.
      *split.
       --split.
         ++apply R2_coref in H0.
           apply R3_coref in H.
           rewrite H0 in H.
           rewrite <- H in H1.
           assumption.
         ++apply R1_coref in H1.
           apply R3_coref in H.
           rewrite <- H in H0.
           rewrite H1 in H0.
           assumption.
       --apply R1_coref in H1.
         apply R2_coref in H0.
         rewrite H0 in H.
         rewrite H1 in H.
         assumption.
      *assumption.
  Qed.

  Lemma lema_ii:
      (((¬) Q5) <∘> dn_L) = (dn_L <∘> ((¬) betaCoref a) <∘> ((¬) Q5)).
  Proof.
    intros.
    rewrite Neg_QDest.
    rewrite <- R1_dnL.
    rewrite <- opA with R1 dn_L (R1 ∩ R2 ∩ R3).
    rewrite <- Neg_QDest.
    rewrite <- R2_dnL.
    rewrite <- R3_dnL.
    transitivity (((¬) Q5) <∘> dn_L).
    -rewrite R3_dnL;reflexivity.
    -destruct_eqRel.
     +rewrite Neg_QDest in H.
      do 4 destruct H.
      exists y0;split.
      *assumption.
      *exists x;split.
       --apply R1_coref in H.
         rewrite H in H2.
         rewrite H.
         assumption.
       --exists y0;split;assumption.
     +rewrite Neg_QDest.
      do 2 destruct H.
      do 2 destruct H0.
      do 2 destruct H1.
      exists x2;split.
      *split.
       --split.
         ++apply R2_coref in H0.
           apply R3_coref in H1.
           rewrite H0 in H.
           rewrite H1 in H.
           assumption.
         ++apply R1_coref in H.
           apply R3_coref in H1.
           rewrite <- H in H0.
           rewrite H1 in H0.
           assumption.
       --apply R1_coref in H.
         apply R2_coref in H0.
         rewrite <- H0 in H1.
         rewrite <- H in H1.
         assumption.
      *assumption.
  Qed.
  
  Hypothesis noInfyZipper: ∀ (S:relation (zipper A))
                             (x:zipper A), ¬infiStar S x.     

  Lemma ec5_1_pre_i_ii:
    (((¬) Q5) <∘> C5)
      ⊆
      ((dn_L <∘> ((¬) (betaCoref a)) <∘> ((¬) Q5))
         ∪
         (dn_R <∘> ((¬) Q5) <∘> C5))
    →
    (((¬) Q5) <∘> C5)
      ⊆
      (C5 <∘> ((¬) (betaCoref a)) <∘> ((¬) Q5)).
  Proof.
    (*Porque truena la induccion y es necesaria coinduccion.
    intros;unfold inclusion;intros.
    unfold C5,C in H.
    rewrite opA in H.
    rewrite cRCC with (zipper A) ((¬) Q5) (dn_R <*>) in H.
    -rewrite <- opA in H.
     rewrite crt_crtCompDer in H.
     induction H.
     +rewrite <- opA with C5 ((¬) (betaCoref a)) ((¬) Q5).
      unfold C5,C.
      rewrite <- opA with (dn_R <*>)
                       dn_L
                       (((¬) (betaCoref a)) <∘> ((¬) Q5)).
      exists x;split.
      *constructor 1;reflexivity.
      *)
    intros.
    rewrite <- opA with C5 ((¬) (betaCoref a)) ((¬) Q5).
    unfold C5,C.
    rewrite <- opA with (dn_R <*>)
                        dn_L
                        (((¬) (betaCoref a)) <∘> ((¬) Q5)).
    rewrite crt_crtCompDer with
        (zipper A)
        dn_R
        (dn_L <∘> (((¬) (betaCoref a)) <∘> ((¬) Q5))).
    rewrite CLOSURE_UEP_2 with
        (zipper A)
        dn_R
        (dn_L
           <∘> (((¬) betaCoref a) <∘> ((¬) Q5))).
    -apply nuind.
     rewrite opA with dn_L ((¬) betaCoref a) ((¬) Q5).
     rewrite opA with dn_R ((¬) Q5) C5.
     assumption.
    -apply noInfyZipper.
  Qed.

  Lemma lema_iii:
    (((¬) Q5) <∘> C5) = (C5 <∘> ((¬) betaCoref a) <∘> ((¬) Q5)).
  Proof.
    intros;destruct_eqRel.
    -revert H;revert x y.
     apply ec5_1_pre_i_ii.
     unfold inclusion;intros.
     rewrite <- lema_i.
     rewrite <- lema_ii.
     do 2 destruct H.
     unfold C5,C in H0;rewrite crt_crtCompDer in H0.
     destruct H0.
     +left;exists x0;split;assumption.
     +rewrite <- crt_crtCompDer in H1.
      right;exists y;split.
      *exists x0;split;assumption.
      *assumption.
    -unfold C5,C in H;do 2 rewrite <- opA in H.
     rewrite crt_crtCompDer in H.
     induction H.
     +rewrite opA in H;rewrite <- lema_ii in H.
      do 2 destruct H;exists x0;split.
      *assumption.
      *exists x0;split.
       --constructor 1;reflexivity.
       --assumption.
     +clear H0;destruct IHcd;destruct H0.
      assert ((dn_R <∘> ((¬) Q5)) x x0).
      *exists y;split;assumption.
      *clear H H0.
       rewrite <- lema_i in H2.
       destruct H2;destruct H.
       exists x1;split.
       --assumption.
       --do 2 destruct H1.
         exists x2;split.
         ++constructor 2 with x0;assumption.
         ++assumption.
  Qed.

  Lemma subEc_5_1:
    (((C5 <∘> ((¬) betaCoref a)) <+>) <∘> ((¬) Q5)) =
    (((¬) Q5) <∘> (C5 <+>)).
  Proof.
    intros;destruct_eqRel.
    -rewrite dnrS_dnl_NbetaA_m in H.
     do 2 rewrite <- opA in H.
     rewrite crt_crtCompDer in H.
     induction H.
     +assert (((C5 <∘> ((¬) betaCoref a)) <∘> ((¬) Q5)) x y).
      *do 2 destruct H.
       rewrite <- opA.
       exists x0;split.
       --exists x;split.
         ++constructor 1;reflexivity.
         ++assumption.
       --assumption.
      *clear H.
       rewrite <- lema_iii in H0.
       destruct H0;destruct H.
       exists x0;split.
       --assumption.
       --exists y;split.
         ++assumption.
         ++constructor 1;reflexivity.
     +clear H0.
      destruct H.
      *rewrite opA in IHcd.
       destruct IHcd;destruct H0.
       rewrite lema_iii in H0.
       rewrite opA.
       exists x0;split.
       --rewrite lema_iii.
         do 2 destruct H0.
         exists x1;split.
         ++do 2 destruct H0.
           exists x2;split.
           **do 2 destruct H0.
             exists x3;split.
             ---constructor 2 with y;assumption.
             ---assumption.
           **assumption.
         ++assumption.
       --assumption.
      *destruct IHcd;destruct H0.
       assert ((C5 <∘> ((¬) betaCoref a) <∘> ((¬) Q5)) x x0).
       --exists y;split.
         ++do 2 destruct H.
           exists x1;split.
           **exists x;split.
             ---constructor 1;reflexivity.
             ---assumption.
           **assumption.
         ++assumption.
       --clear H H0.
         rewrite <- lema_iii in H2.
         destruct H2;destruct H.
         exists x1;split.
         **assumption.
         **exists x0;split.
           ---assumption.
           ---do 2 destruct H1.
              constructor 2 with x2;assumption.
    -rewrite opA in H;rewrite crt_crtCompIzq in H.
     induction H.
     +rewrite lema_iii in H.
      rewrite <- opA.
      do 2 destruct H.
      exists x0;split.
      *assumption.
      *exists x0;split.
       --constructor 1;reflexivity.
       --assumption.
     +clear H.
      destruct IHci;destruct H.
      assert ((((¬) Q5) <∘> C5) x0 z).
      *exists y;split;assumption.
      *clear H0 H1.
       rewrite lema_iii in H2.
       destruct H2;destruct H0.
       exists x1;split.
       --do 2 destruct H.
         exists x2;split.
         ++assumption.
         ++rewrite crt_eq_crt2;constructor 2 with x0.
           **rewrite <- crt_eq_crt2;assumption.
           **assumption.
       --assumption.
  Qed.

  Theorem ec_5_1:
    (((¬) Q5) <∘> (betaCoref b) <∘> (C5 <+>)) =
    ((betaCoref b) <∘> ((C5 <∘> ((¬) betaCoref a) <+>)) <∘> ((¬) Q5)).
  Proof.
    intros.
    rewrite cRCC with (zipper A)
                      ((¬) Q5) (betaCoref b).
    -rewrite <- opA.
     rewrite <- subEc_5_1.
     rewrite <- opA with (betaCoref b)
                         ((C5 <∘> ((¬) betaCoref a) <+>))
                         ((¬) Q5).
     reflexivity.
    -apply NEG_COREFL.
    -apply betaCoref_coref.
  Qed.

  Lemma Q_idTop:
    (((¬) Q5) <∘> (id <X> topCoref)) = (id <X> topCoref).
  Proof.
    intros.
    rewrite <- COREFL_COMP.
    -destruct_eqRel.
     +destruct H;assumption.
     +split.
      *rewrite <- NEG_DBLNEG with
           (zipper A)
           (id <X> topCoref) in H.
       --revert H;revert x y.
         apply GC_DIFF.
         unfold inclusion;intros.
         rewrite <- NEG_DEMORGAN_2.
         split.
         ++assumption.
         ++unfold not;intros.
           destruct H0.
           destruct H0.
           rewrite H0 in H1.
           destruct H2.
           inversion H1.
           **do 2 destruct H2.
             do 2 destruct H5.
             inversion H6.
             inversion H11.
           **destruct H2.
             ---rewrite <- H2 in H5.
                do 2 destruct H5.
                inversion H8.
                inversion H13.
             ---destruct H2.
                +++rewrite <- H2 in H5.
                   do 2 destruct H5.
                   inversion H8.
                   inversion H13.
                +++contradiction.
       --unfold coref;unfold inclusion;intros.
         destruct H0.
         inversion H1.
         rewrite H0.
         reflexivity.
      *assumption.
    -apply NEG_COREFL.
    -unfold coref;unfold inclusion;intros.
     destruct H.
     rewrite H;inversion H0;reflexivity.
  Qed.

  Theorem ec5_2:
    ie (\ (desc ;; b [[ negation (pathP (anc ;; a)) ]])) =
    ((betaCoref b)
       <∘> ((C5 <∘> ((¬) betaCoref a)) <+>)
       <∘> (id <X> topCoref)
       <∘> ((up_L ∪ up_R) <*>)).
  Proof.
    intros;simpl.
    rewrite upl_uprcrt_cm.
    rewrite opA with (betaCoref a)
                     up_L ((uniones [@up_L A;@up_R A]) <*>).
    rewrite SYMCLS_DOM.
    -rewrite dombetaCoref_upl.
     rewrite opA with ((¬) Q5) (betaCoref b) (C5 <+>).
     rewrite ec_5_1.
     rewrite <- opA with
         (betaCoref b <∘> ((C5 <∘> ((¬) betaCoref a)) <+>))
         ((¬) Q5) (id <X> topCoref).
     rewrite Q_idTop.
     reflexivity.
    -apply listIny_upL_upR.
  Qed.

End Sec5.