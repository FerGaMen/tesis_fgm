(*
* Implementación de álgeras de Bool.
*
* Desarrollado por Fernando Abigail Galicia Mendoza
* Contacto: fernandogamen@ciencias.unam.mx
*
* Trabajo desarrollado bajo el marco del proyecto:
* "Tópicos en computación teórica" (PAPIME, PE102117)
* Otorgado por la DGAPA de la UNAM
*
* Coq v8.7
* OCaml v4.05.0
* Referencias:
* 1. Huntington: 
*    "Sets of independent postulates for the algebra of logic"
* 2. Whitesitt:
*    "Boolean algebra and its applications"
*)

Require Export Utf8.

(*
* Definimos la estructura de un álgebra booleana
* que consiste en una operación complemento,
* dos operaciones binarias con un neutro cada una.
*)
Class AlgBool
      (A:Type)
      (uno cero:A)
      (neg:A → A)
      (disy conju:A → A → A)
  :={
      dN: ∀ (x:A),
        disy x cero = x;
      dC: ∀ (x y:A),
          disy x y = disy y x;
      cN: ∀ (x:A),
          conju x uno = x;
      cC: ∀ (x y:A),
          conju x y = conju y x;
      dDC: ∀ (x y z:A),
          disy x (conju y z) = conju (disy x y) (disy x z);
      dCD: ∀ (x y z:A),
          conju x (disy y z) = disy (conju x y) (conju x z);
      dInv: ∀ (x:A),
          disy x (neg x) = uno;
      cInv: ∀ (x:A),
          conju x (neg x) = cero
    }.

Hint Resolve dN.
Hint Resolve cN.
Hint Resolve dDC.
Hint Resolve dCD.
Hint Resolve dInv.
Hint Resolve cInv.

(*
* Los siguientes teoremas son dados por Huntington y
* Whitesitt en sus obras.
* A pesar de que ambos escritos son especificos
* en sus pasos, observaremos que suposiciones del estilo:
* "De ahora en adelante denotaremos a x(yz) y (xy)z como xyz."
* No son admisibles en Coq.
* También demostraciones para unicidad y uso de estas, la idea es la misma
* pero la forma de pensar debe ser distinta.
*)
Section AB_Hechos.

  Generalizable Variables A uno cero neg disy conju.

  Context `{AB:AlgBool A uno cero neg disy conju}.

  Local Notation "−" := neg.
  Local Infix "+" := disy.
  Local Infix "∙" := conju (at level 80).
  Local Notation "1'" := uno.
  Local Notation "0'" := cero.

  (*Unicidad del neutro disyuntivo*)
  Theorem cU: exists! (c:A), ∀ (x:A), x + c = x.
  Proof with eauto.
    exists cero;split...
    -intros x' H;rewrite <- dN with x'.
     rewrite dC...
  Qed.

  Hint Resolve cU.

  (*Unicidad del neutro conjuntivo*)
  Theorem uU: exists! (u:A), ∀ (x:A), (x ∙ u) = x.
  Proof with eauto.
    exists uno;split...
    -intros x' H;rewrite <- cN with x'.
     rewrite cC...
  Qed.

  Hint Resolve uU.

  (*Involución de la disyunción*)
  Theorem dI: ∀ (x:A), x + x = x.
  Proof with eauto.
    intros;rewrite <- dN.
    assert (H := cInv);rewrite <- H with x...
    rewrite dDC;rewrite dInv.
    assert (H0 := cN)...
  Qed.

  Hint Resolve dI.

  (*Involución de la conjunción*)
  Theorem cI: ∀ (x:A), (x ∙ x) = x.
  Proof with eauto.
    intros;rewrite <- cN.
    assert (H := dInv);rewrite <- H with x.
    rewrite dCD;rewrite cInv.
    assert (H0 := dN)...
  Qed.

  Hint Resolve cI.
  
  (*Supremo*)
  Theorem dSup: ∀ (x:A), x + uno = uno.
  Proof with eauto.
    intros;symmetry;transitivity (x + (− x)).
    -symmetry...
    -rewrite <- cN with (− x);rewrite dDC;rewrite dInv.
     rewrite cC...
  Qed.

  Hint Resolve dSup.

  (*Ínfimo*)
  Theorem cInf: ∀ (x:A), (x ∙ cero) = cero.
  Proof with eauto.
    intros;symmetry;transitivity (x ∙ (− x)).
    -symmetry...
    -rewrite <- dN with (− x);rewrite dCD;rewrite cInv.
     rewrite dC...
  Qed.

  Hint Resolve cInf.

  (*Ley de absorción para la disyunción*)
  Theorem dAbs: ∀ (x y:A), x + (x ∙ y) = x.
  Proof with eauto.
    intros;transitivity ((x ∙ uno) + (x ∙ y)).
    -assert (H := cN);rewrite H...
    -rewrite <- dCD;assert (H := dC);rewrite H.
     rewrite dSup...
  Qed.

  Hint Resolve dAbs.

  (*Ley de absorción para la conjunción*)
  Theorem cAbs: ∀ (x y:A), (x ∙ (x + y)) = x.
  Proof with eauto.
    intros;transitivity ((x + cero) ∙ (x + y)).
    -assert (H := dN);rewrite H...
    -symmetry;rewrite <- dDC;assert (H := cC);rewrite H.
     rewrite cInf;rewrite dN...
  Qed.

  Hint Resolve cAbs.

  (*
   *El inverso de la disyunción y conjunción, es únicamente 
   *determinado por su complemento
   *)
  Theorem dCInvU: ∀ (x:A),
    exists! (x':A), x + x' = uno /\ (x ∙ x') = cero.
  Proof with eauto.
    intros;exists (neg x);split...
    intros;destruct H;symmetry...
    rewrite <- cN with x'.
    rewrite cC;assert (H1 := dInv);rewrite <- H1 with x.
    rewrite cC;rewrite dCD;rewrite cC in H0.
    rewrite H0;assert (H2 := cInv);rewrite <- H2 with x.
    assert (H3 := cC);rewrite H3;rewrite H3 with x' (neg x).
    rewrite <- dCD;rewrite H...
  Qed.

  Hint Resolve dCInvU.

  (*La negación es inversa de si misma*)
  Theorem nI: ∀ (x:A), − (− x) = x.
  Proof with eauto.
    intros;destruct dCInvU with (− x);destruct H.
    destruct H;destruct H0 with (− (− x))...
    symmetry;rewrite <- cN with x0;rewrite cC.
    assert (H2 := dInv);rewrite <- H2 with x;rewrite cC.
    rewrite dCD;rewrite cC in H1;rewrite H1.
    assert (H3 := cInv);rewrite <- H3 with x.
    rewrite cC with x0 x;rewrite <- dCD;rewrite dC with x0 (− x).
    rewrite H;rewrite cN...
  Qed.

  Hint Resolve nI.

  (*
   * Las siguientes ecuaciones son dadas por Whitesitt para demostrar la 
   * asociatividad de la conjunción y disyunción.
   *)
  Lemma ec1: ∀ (x y z:A),
      ((x + (x ∙ (y ∙ z))) ∙ ((− x) + (x ∙ (y ∙ z)))) = (x ∙ (y ∙ z)).
  Proof with eauto.
    intros;rewrite dC with x (x ∙ (y ∙ z)).
    rewrite dC with (− x) (x ∙ (y ∙ z));rewrite <- dDC.
    rewrite cInv...
  Qed.

  Hint Resolve ec1.

  Lemma ec2: ∀ (x y z:A),
      ((x + ((x ∙ y) ∙ z)) ∙ ((− x) + ((x ∙ y) ∙ z))) = ((x ∙ y) ∙ z).
  Proof with eauto.
    intros;rewrite dC with x ((x ∙ y) ∙ z).
    rewrite dC with (− x) ((x ∙ y) ∙ z);rewrite <- dDC.
    rewrite cInv...
  Qed.

  Hint Resolve ec2.

  Lemma ec3: ∀ (x y z:A), x + (x ∙ (y ∙ z)) = x + ((x ∙ y) ∙ z).
  Proof with eauto.
    intros;rewrite dAbs;transitivity (x ∙ (x + z)).
    -rewrite <- cN with x...
    -rewrite dDC with x (x ∙ y) z;rewrite dAbs...
  Qed.

  Hint Resolve ec3.

  Lemma ec4: ∀ (x y z:A), (− x) + (x ∙ (y ∙ z)) = (− x) + ((x ∙ y) ∙ z).
  Proof with eauto.
    intros;transitivity ((x + (− x)) ∙ ((− x) + (y ∙ z))).
    -rewrite dDC;rewrite dC with (− x) x;trivial.
    -rewrite dInv;rewrite cC with uno (− x + (y ∙ z));rewrite cN.
     rewrite dDC;symmetry.
     transitivity (((− x) + (x ∙ y)) ∙ ((− x) + z)).
     +rewrite <- dDC...
     +assert (H := dDC);rewrite H.
      rewrite dC with (− x) x.
      rewrite dInv with x;rewrite cC with uno (− x + y).
      rewrite cN with (− x + y)...
  Qed.

  Hint Resolve ec4.

  Lemma ec5: ∀ (x y z:A),
      ((x ∙ (x + (y + z))) + ((− x) ∙ (x + (y + z)))) = (x + (y + z)).
  Proof with eauto.
    intros;rewrite cC with x (x + (y + z)).
    rewrite cC with (− x) (x + (y + z));rewrite <- dCD.
    rewrite dInv...
  Qed.

  Hint Resolve ec5.

  Lemma ec6: ∀ (x y z:A),
      ((x ∙ ((x + y) + z)) + ((− x) ∙ ((x + y) + z))) = ((x + y) + z).
  Proof with eauto.
    intros;rewrite cC with x ((x + y) + z).
    rewrite cC with (− x) ((x + y) + z);rewrite <- dCD.
    rewrite dInv...
  Qed.

  Hint Resolve ec6.

  Lemma ec7: ∀ (x y z:A), (x ∙ (x + (y + z))) = (x ∙ ((x + y) + z)).
  Proof with eauto.
    intros;rewrite cAbs;transitivity (x + (x ∙ z))...
    rewrite dCD with x (x + y) z;rewrite cAbs...
  Qed.

  Hint Resolve ec7.

  Lemma ec8: ∀ (x y z:A),
      ((− x) ∙ (x + (y + z))) = ((− x) ∙ ((x + y) + z)).
  Proof with eauto.
    intros;transitivity ((x ∙ (− x)) + ((− x) ∙ (y + z))).
    -rewrite dCD;rewrite cC with (− x) x...
    -rewrite cInv;rewrite dC with cero (− x ∙ (y + z)).
     rewrite dN;rewrite dCD.
     symmetry;transitivity (((− x) ∙ (x + y)) + ((− x) ∙ z))...
     +assert ((− x ∙ (x + y)) = ((− x ∙ x) + (− x ∙ y)))...
      rewrite H;rewrite cC with (− x) x.
      rewrite cInv with x;rewrite dC with cero (− x ∙ y).
      rewrite dN with (− x ∙ y)...
  Qed.

  Hint Resolve ec8.

  (*La conjunción es asociativa*)
  Theorem cAso: ∀ (x y z:A), (x ∙ (y ∙ z)) = ((x ∙ y) ∙ z).
  Proof with eauto.
    intros;rewrite <- ec1;rewrite ec3;rewrite ec4...
  Qed.

  Hint Resolve cAso.

  (*La disyunción es asociativa*)
  Theorem dAso: ∀ (x y z:A), (x + (y + z)) = ((x + y) + z).
  Proof with eauto.
    intros;rewrite <- ec5;rewrite ec7;rewrite ec8...
  Qed.

  Hint Resolve dAso.

  (*El inverso de la conjunción es la disyunción negada*)
  Theorem cInvD: ∀ (x y:A), − (x ∙ y) = (− x + − y).
  Proof with eauto.
    intros;destruct dCInvU with (x ∙ y);destruct H;destruct H.
    destruct H0 with (− x + − y).
    -split.
     +rewrite dC;rewrite dDC;rewrite dC with (− x + − y) x.
      rewrite dAso;rewrite dInv;rewrite dC with uno (− y).
      rewrite dSup;rewrite cC;rewrite cN.
      rewrite <- dAso;rewrite dC with (− y) y.
      rewrite dInv...
     +rewrite dCD;rewrite cC with (x ∙ y) (− x);rewrite cAso.
      rewrite cC with (− x) x;rewrite cInv;rewrite cC with cero y.
      rewrite cInf;rewrite dC;rewrite dN;rewrite cC with x y.
      rewrite cC with (y ∙ x) (− y);rewrite cAso.
      rewrite cC with (− y) y;rewrite cInv.
      rewrite cC with cero x...
    -symmetry;apply H0...
  Qed.

  Hint Resolve cInvD.

  (*El inverso de la disyunción es la conjunción negada*)
  Theorem dInvC: ∀ (x y:A), − (x + y) = (− x ∙ − y).
  Proof with eauto.
    intros;destruct dCInvU with (x + y);destruct H;destruct H.
    destruct H0 with (− x ∙ − y).
    -split.
     +rewrite dDC with (x + y) (− x) (− y).
      rewrite dC with (x + y) (− x);rewrite dAso.
      rewrite dC with (− x) x;rewrite dInv;rewrite dC with uno y.
      rewrite dSup;rewrite cC;rewrite cN.
      rewrite <- dAso;rewrite dInv...
     +rewrite cC with (x + y) (− x ∙ − y);rewrite dCD.
      rewrite cC with (− x ∙ − y) x;rewrite cAso.
      rewrite cInv;rewrite cC with cero (− y);rewrite cInf.
      rewrite dC;rewrite dN;rewrite <- cAso.
      rewrite cC with (− y) y;rewrite cInv...
    -symmetry;apply H0...
  Qed.

  Hint Resolve dInvC.

  (*La conjunción es dual a la disyunción*)
  Theorem cDualD: ∀ (x y:A), (x ∙ y) = − (− x + − y).
  Proof with eauto.
    intros;rewrite <- nI with (x ∙ y);rewrite cInvD...
  Qed.

  Hint Resolve cDualD.

  (*La disyunción es dual a la conjunción*)
  Theorem dDualC: ∀ (x y:A), (x + y) = − (− x ∙ − y).
  Proof with eauto.
    intros;rewrite <- nI with (x + y);rewrite dInvC...
  Qed.

  Hint Resolve dDualC.
  
End AB_Hechos.