(*
* Implementación de relaciones binarias y 
* sus operadores.
*
* Desarrollado por Fernando Abigail Galicia Mendoza
* Contacto: fernandogamen@ciencias.unam.mx
* Coq v8.7
* OCaml v4.05.0
*
* Trabajo desarrollado bajo el marco del proyecto:
* "Tópicos en computación teórica" (PAPIME, PE102117)
* Otorgado por la DGAPA de la UNAM
*
* Referencias:
* 1. Ikeda y Nishimura:
*    "Calculating tree navigation with symmetric relational zipper."
*)

Require Export Lib.AlgRel.AR.
Require Export Relations.
Require Export List.
Require Import Utf8.
Import ListNotations.
Open Scope list_scope.

Set Implicit Arguments.

(*
* Notaciones no brindadas por la biblioteca
* Relation_Definitions.
*)
Notation "R ⊆ S" := (inclusion _ R S) (at level 80).

Notation "R ≡ S" := (same_relation _ R S) (at level 80).

(*Axioma de igualdad de relaciones binarias.*)
Axiom eqRelEq: ∀ (A:Type) (R S:relation A), R = S ↔ (R ≡ S).

(*Relación vacia*)
Inductive vacia {A:Type} : relation A := .

(*Relación total.*)
Inductive total {A:Type} : relation A :=
| t: ∀ (x y:A), total x y.

Hint Constructors total.

(*Unión de dos relaciones.*)
Inductive union {A:Type} (R S:relation A) : relation A :=
| uI: ∀ (x y:A), R x y  → (union R S) x y
| uD: ∀ (x y:A), S x y  → (union R S) x y.

Infix "∪" := union (at level 80).

Hint Constructors union.

(*Complemento de una relación.*)
Definition compl {A:Type} (R:relation A) : relation A :=
  λ (x y:A), ¬R x y.

Notation "R ^" := (compl R) (at level 80).

Hint Resolve compl.

(*Intersección de relaciones.*)
Inductive inter {A:Type} (R S:relation A) : relation A :=
| i: ∀ (x y:A), R x y → S x y → (inter R S) x y.

Infix "∩" := inter (at level 80).

Hint Constructors inter.

(*Inversa de una relación.*)
Definition inv {A:Type} (R:relation A) : relation A :=
  λ (x y:A), R y x.

Notation "R ~" := (inv R) (at level 80).

Hint Resolve inv.

(*Composición de relaciones.*)
Definition comp {A:Type} (R S:relation A) : relation A :=
  λ (x y : A), ∃ (z : A), R x z ∧ S z y.

Infix "<∘>" := comp (at level 80).

Hint Resolve comp.

(*Relación identidad.*)
Definition id {A:Type} : relation A :=
  λ (x y : A), x = y.

Hint Resolve id.

(*Predicado que indica si una relación es simple.*)
Definition simple {A:Type} (R:relation A) : Prop :=
  (R <∘> (R ~)) ⊆ id.

(*Predicado que indica si una relación es inyectiva.*)
Definition iny {A:Type} (R:relation A) : Prop :=
  ((R ~) <∘> R) ⊆ id.

(*Predicado que indica si una relación es entera.*)
Definition entera {A:Type} (R:relation A) : Prop :=
  id ⊆ ((R ~) <∘> R).

(*Predicado que indica si una relación es suprayectiva.*)
Definition supr {A:Type} (R:relation A) : Prop :=
  id ⊆ (R <∘> (R ~)).

(*
* Predicado que indica que una relación
* binaria sea coreflexiva.
*)
Definition coref {A:Type} (R:relation A) : Prop :=
  R ⊆ id.

(*Producto de relaciones.*)
Inductive produ {A B:Type} (R:relation A) (S:relation B) :
  relation (A*B) :=
| pI: ∀ (x y:A) (w z:B), R x y → S w z → (produ R S) (x,w) (y,z).

Infix "<X>" := produ (at level 80).

Hint Constructors produ.

(*Diferencia de relaciones.*)
Inductive diff {A:Type} (R S:relation A) : relation A :=
| d: ∀ (x y:A), R x y → ¬S x y → (diff R S) x y.

Infix "\" := diff (at level 80).

Hint Constructors diff.

(*Dominio de una relación*)
Definition dom {A:Type} (R:relation A) : relation A :=
  id ∩ ((R ~) <∘> R).

(*Propiedad de que una relación sea transitiva.*)
Definition RTrans {A:Type} (X:relation A) : Prop :=
  ∀ (x y z:A), X x y -> X y z -> X x z.

(*Propiedad de que una relación sea reflexiva.*)
Definition RRefl {A:Type} (X:relation A) : Prop :=
  ∀ (x:A), X x x.

(*Distintas definiciones de la cerradura reflexiva-transitivia.*)

(*Definición 1.*)
Inductive cerr_refl_trans {A:Type} (R:relation A) : relation A :=
| crt1 (x y:A) : id x y → (cerr_refl_trans R) x y
| crt2 (x y z:A): R x y → (cerr_refl_trans R) y z
                      → (cerr_refl_trans R) x z.

Notation "R <*>" := (cerr_refl_trans R) (at level 80).

(*Definición 2.*)
Inductive cerr_refl_trans2 {A:Type} (R:relation A) : relation A :=
| crt2_1 (x y:A) : id x y → (cerr_refl_trans2 R) x y
| crt2_2 (x y z:A): (cerr_refl_trans2 R) x y
                       → R y z
                       → (cerr_refl_trans2 R) x z.

Notation "R <*2>" := (cerr_refl_trans2 R) (at level 80).

Notation "R <+>" := (R <∘> (R <*>)) (at level 80).

(*
* Composición por la derecha de una relación con la cerradura de otra, no necesariamente
* distinta.
*)
Inductive cd {A:Type} (R S:relation A) : relation A :=
| compDer1 (x y:A) : S x y → (cd R S) x y
| compDer2 (x y z:A): R x y → (cd R S) y z
                        → (cd R S) x z.

(*
* Composición por la izquierda de una relación con la cerradura de otra, no necesariamente
* distinta.
*)
Inductive ci {A:Type} (S R:relation A) : relation A :=
| compIzq1 (x y:A) : S x y → (ci S R) x y
| compIzq2 (x y z:A): (ci S R) x y → R y z
                        → (ci S R) x z.

(*Predicado que indica si una lista contiene puras relaciones inyectivas.*)
Definition listIny {A:Type} (l:list (relation A)) :=
  ∀ (S:relation A), In S l → iny S.

(*Función que realiza la union de todas las Si almacenadas en una lista.*)
Fixpoint uniones {A:Type} (l:list (relation A)) : relation A :=
  match l with
  | [] => vacia
  | [r] => r
  | (r::rs) => r ∪ (uniones rs)
  end.

(*
 * Función que realiza la unión de la forma Si ~ con Si, con Si contenida en
 * una lista.
*)
Fixpoint unComp {A:Type} (Ss:list (relation A)) :=
  match Ss with
  | [] => vacia
  | [Si] => (Si ~) <∘> Si
  | (Si::Sss) => ((Si ~) <∘> Si) ∪ (unComp Sss)
  end.

(*
 * Función que realiza la unión de la forma Si ~ con R y Si, con Si contenida en
 * una lista.
*)
Fixpoint unCompR {A:Type} (R:relation A)
         (Ss:list (relation A)) :=
  match Ss with
  | [] => vacia
  | [Si] => (Si ~) <∘> R <∘> Si
  | (Si::Sss) => ((Si ~) <∘> R <∘> Si) ∪ (unCompR R Sss)
  end.

(*Definición de la cerradura simétrica. [IN11]*)
Inductive cerr_symmetric {A:Type} (R:relation A)
  (Ss:list (relation A)) : relation A:=
| cs_r (x y:A): R x y → (cerr_symmetric R Ss) x y
| cs_l (x y z w:A) (Si:relation A): In Si Ss →
    (Si ~) x y → (cerr_symmetric R Ss) y z → Si z w →
    (cerr_symmetric R Ss) x w.

Notation "R L <^>" := (cerr_symmetric R L) (at level 80).